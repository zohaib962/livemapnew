package com.example.liveearthmap

import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.liveearthmap.other.RemoteKeys
import com.example.liveearthmap.other.SharePrefData
import kotlinx.android.synthetic.main.confirm_dialog.*
import java.util.ArrayList

class ConfirmDialog(internal var _activity: Activity, var clickInterface: ClickInterface) : Dialog(_activity) {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.confirm_dialog)


        navigate.setOnClickListener {
            dismiss()
            clickInterface.clicked()
        }

        cancelImg.setOnClickListener {
            dismiss()
        }



    }

    interface ClickInterface{
        fun clicked()
    }


}