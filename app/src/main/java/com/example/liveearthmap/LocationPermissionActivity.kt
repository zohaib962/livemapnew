package com.example.liveearthmap

import android.Manifest
import android.animation.Animator
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import com.example.liveearthmap.other.*
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.activity_location_permission.*
import kotlinx.android.synthetic.main.activity_location_permission.admobNativeView
import kotlinx.android.synthetic.main.activity_location_permission.mainAdLayout
import java.util.ArrayList


class LocationPermissionActivity : AppCompatActivity() {
     var locationPermissionActivity: LocationPermissionActivity = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }*/
        locationPermissionActivity == this
        setContentView(R.layout.activity_location_permission)

        textView.setOnClickListener {
            mainAdLayout.visibility=View.GONE
            admobNativeView.visibility=View.GONE
            animationLayout.visibility = View.VISIBLE

            av_from_code.setAnimation("splash_main.json")
            av_from_code.playAnimation()

            av_from_code.addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    animationLayout.visibility = View.GONE

                    val permissions = arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                    Permissions.check(
                        this@LocationPermissionActivity /*context*/,
                        permissions,
                        null /*rationale*/,
                        null /*options*/,
                        object : PermissionHandler() {
                            override fun onGranted() {
                                // do your task.
                                startActivity(
                                    Intent(
                                        this@LocationPermissionActivity,
                                        MainActivity::class.java
                                    )
                                )
                                finish()

                            }

                            override fun onDenied(
                                context: Context?,
                                deniedPermissions: ArrayList<String>?
                            ) {

                                AlertDialog.Builder(this@LocationPermissionActivity)
                                    .setTitle("Permission denied")
                                    .setMessage("Some function cannot work properly. The application need your location.") // Specifying a listener allows you to take an action before dismissing the dialog.
                                    // The dialog is automatically dismissed when a dialog button is clicked.
                                    .setPositiveButton(
                                        "Ok"
                                    ) { dialog, which ->
                                        dialog.dismiss()
                                    }

                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show()

                                super.onDenied(context, deniedPermissions)
                            }


                        })


                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

            })


        }

        NativeAdsApplovin.showNativeAd(this,this,RemoteKeys.lem_permission_native_new,admobNativeView,object :
            NativeAppLovinCallback {
            override fun onLoaded() {
                mainAdLayout.visibility=View.VISIBLE
            }

            override fun onFailed() {
                mainAdLayout.visibility=View.GONE
            }

        })


        noAdView.setOnClickListener {
            startActivity(Intent(this,AdRemoveActivity::class.java))
        }


    }

}