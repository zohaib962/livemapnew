package com.example.liveearthmap

import android.Manifest
import android.R
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.example.liveearthmap.adapter.AutoCompleteAdapter
import com.example.liveearthmap.model.PlacesDetail
import com.example.liveearthmap.network.AlitudeResponse
import com.example.liveearthmap.network.ApiInterfave
import com.example.liveearthmap.network.RetrofitController
import com.example.liveearthmap.other.Api
import com.example.liveearthmap.other.MyLocation
import com.example.liveearthmap.other.Parser
import com.example.liveearthmap.other.Utils
import com.example.liveearthmap.other.Utils.hideKeyboard
import com.example.liveearthmap.other.Utils.vectorToBitmap
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_share_address.*
import kotlinx.android.synthetic.main.search_location.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.lang.IndexOutOfBoundsException
import java.lang.NullPointerException
import java.util.*
import kotlin.collections.ArrayList


abstract class LocationActivity : BaseActivity(), MyLocation.OnLocationUpdate {

    private val RECORD_REQUEST_CODE: Int = 1;
    protected var myLocation: MyLocation? = null

    var mMap: GoogleMap? = null
    var location: Location? = null
    protected var isShow: Boolean = false


    abstract fun updateLocation(location: Location?)
    abstract fun updateLocationAltidtude(location: LatLng?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startLocation()
    }

    fun startLocation(){
        if (isGranted()) {

            myLocation = MyLocation(applicationContext, this,this)
            myLocation!!.start()

        }
    }

    override fun onStart() {
        super.onStart()
        voice.setOnClickListener {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
            intent.putExtra(
                RecognizerIntent.EXTRA_PROMPT,
                "Say something..."
            )
            try {
                startActivityForResult(intent, 0)
            } catch (a: ActivityNotFoundException) {
                Toast.makeText(
                    applicationContext,
                    "Sorry! Your device dosn\'t support speech input",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        search.setOnClickListener {
            Utils.hideKeyboard(this)
            searchAddress()

        }
        if (autoCompleteTextView != null)
            autoCompleteTextView.setOnEditorActionListener(object :
                TextView.OnEditorActionListener {
                override fun onEditorAction(
                    v: TextView?,
                    actionId: Int,
                    event: KeyEvent?
                ): Boolean {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        hideKeyboard(this@LocationActivity)
                        searchAddress()
                        return true
                    }
                    return false
                }
            })
    }

    private fun searchAddress() {

        if (autoCompleteTextView.text.toString().trim().isEmpty()) {
            Toast.makeText(applicationContext, "Input address first", Toast.LENGTH_SHORT).show()
            return
        }

        val locationFromAddress = Utils.getLocationFromAddress(
            applicationContext,
            autoCompleteTextView.text.toString().trim()
        )

        if (locationFromAddress == null)
            Toast.makeText(applicationContext, "Can't find address!", Toast.LENGTH_SHORT)
                .show()
        else {
            if (address != null)
                address.text = autoCompleteTextView.text.toString().trim()

            goToLocation(
                locationFromAddress.latitude,
                locationFromAddress.longitude,
                autoCompleteTextView.text.toString().trim()
            )

        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            0 -> {
                if (resultCode === RESULT_OK && null != data) {
                    val result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    autoCompleteTextView.setText(result!![0])


                    if (autoCompleteTextView.text.toString().trim().isEmpty()) {
                        return
                    }

                    val locationFromAddress = Utils.getLocationFromAddress(
                        applicationContext,
                        autoCompleteTextView.text.toString().trim()
                    )

                    if (locationFromAddress == null)
                        Toast.makeText(
                            applicationContext,
                            "Can't find address!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    else {
                        if (address != null)
                            address.text = autoCompleteTextView.text.toString().trim()

                        goToLocation(
                            locationFromAddress.latitude,
                            locationFromAddress.longitude,
                            autoCompleteTextView.text.toString().trim()
                        )

                    }


                }
            }
        }
    }

    override fun locationChange(location: Location?) {
        this.location = location
        if (!isShow){
            goToCurrentLocation()
            updateLocation(location)
        }
        if (myLocation != null)
            myLocation!!.stop()
    }

    override fun connectionSuspended(i: Int) {

    }

    override fun connectionFailed(connectionResult: ConnectionResult?) {
    }


    override fun onDestroy() {
        super.onDestroy()
        if (myLocation != null)
            myLocation!!.stop()
    }


    @SuppressLint("MissingPermission")
    protected open fun goToCurrentLocation() {
        if (mMap == null || location == null) {
            startLocation()
            return
        }
        mMap!!.clear()

        val markerIcon = vectorToBitmap(
            applicationContext
        )


        val coordinate = LatLng(
            location!!.latitude,
            location!!.longitude
        )
        mMap!!.addMarker(
            MarkerOptions().position(coordinate)
                .icon(markerIcon).title("Your location")
        )

        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(
            coordinate, 15f
        )
        mMap!!.animateCamera(cameraUpdate)

        if (address != null) {
            var completeAddressString = Utils.getCompleteAddressString(
                applicationContext,
                location!!.latitude,
                location!!.longitude
            )
            address.text = completeAddressString
        }
        autoCompleteTextView.setText("")


    }


    protected open fun goToLocation(
        latitude: Double,
        longitude: Double,
        s: String,
    ) {


        val sydney = LatLng(latitude, longitude)
        val markerIcon = vectorToBitmap(applicationContext)

        if (mMap != null) {
            mMap!!.clear()
            mMap!!.addMarker(MarkerOptions().position(sydney).icon(markerIcon).title(s))
            val coordinate = LatLng(
                latitude,
                longitude
            ) //Store these lat lng values somewhere. These should be constant.
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(coordinate, 15f)
            mMap!!.animateCamera(cameraUpdate)

            updateLocationAltidtude(coordinate)

            if (location != null) {
                mMap!!.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            location!!.latitude,
                            location!!.longitude
                        )
                    )
                        .icon(markerIcon).title("Your location")
                )

            }


        }


    }


    private fun isGranted(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {


            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                RECORD_REQUEST_CODE
            )

            return false
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RECORD_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    AlertDialog.Builder(applicationContext)
                        .setTitle("Permission denied")
                        .setMessage("Some function cannot work properly. The application need your location.") // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(
                            "Grant"
                        ) { dialog, which ->
                            isGranted()
                            dialog.dismiss()
                        }
                        .setNegativeButton("Cancel", null)
                        .show()
                } else {
                    myLocation = MyLocation(applicationContext, this,this)
                    myLocation!!.start()


                }
            }
        }
    }


    val list = ArrayList<PlacesDetail>()


    protected fun search(s: String) {


        Api.getCall(
            applicationContext,
            "http://api.geonames.org/searchJSON?q=$s&maxRows=10&username=ghayas",
            "",
            object : Api.VolleyCallBack {
                override fun onResponse(response: String?) {
                    try {

                        val jsonObject = JSONObject(response)
                        if (jsonObject.has("geonames")) {

                            val jsonArray = jsonObject.getJSONArray("geonames")

                            list.clear()

                            val searchArrayList = ArrayList<String>()


                            for (i in 0 until jsonArray.length()) {
                                val b = jsonArray.getJSONObject(i)

                                val placesDetail = PlacesDetail()

                                placesDetail.lng = Parser.getString(b, "lng")
                                placesDetail.lat = Parser.getString(b, "lat")
                                placesDetail.name = Parser.getString(b, "name")

                                list.add(placesDetail)

                                searchArrayList.add(placesDetail.name)
                            }

                            val adapter = AutoCompleteAdapter(
                                applicationContext,
                                R.layout.simple_dropdown_item_1line,
                                R.id.text1,
                                searchArrayList
                            )

                            autoCompleteTextView.setAdapter(adapter)
                            autoCompleteTextView.onItemClickListener = onItemClickListener

                        }

                    }catch (e: JSONException){

                    }catch (e:Exception){

                    }
                }

                override fun onError(error: VolleyError?) {
                    Log.d("volleyerror",error.toString())
                }

            })


    }


    private val onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->

        try {
            for (i in 0 until list.size) {
                if (adapterView.getItemAtPosition(i).equals(list[i].name)) {
                    goToLocation(list[i].lat.toDouble(), list[i].lng.toDouble(), list[i].name)

                    if (address != null) {
                        address.text = list[i].name
                    }

                    break

                }
            }
        }catch (e:IndexOutOfBoundsException){

        }catch (e:Exception){

        }
    }

    protected fun setOnMapClick(googleMap: GoogleMap) {
        googleMap.setOnMapClickListener { point ->
            if (!isShow) {
                googleMap.clear()

                val markerIcon = vectorToBitmap(
                    applicationContext
                )

                var completeAddressString = Utils.getCompleteAddressString(
                    applicationContext,
                    point.latitude,
                    point.longitude
                ).replace("Unnamed Road,", "")
                val marker = MarkerOptions()
                    .position(LatLng(point.latitude, point.longitude))
                    .icon(markerIcon)
                    .title(
                        completeAddressString
                    )
                googleMap.addMarker(marker)

                if (address != null)
                    address.text = completeAddressString.trim()
                autoCompleteTextView.setText(completeAddressString)


            }
        }

    }

    protected fun setOnMapClick(googleMap: GoogleMap, textView:TextView) {
        googleMap.setOnMapClickListener { point ->
            if (!isShow) {
                googleMap.clear()

                val markerIcon = vectorToBitmap(
                    applicationContext
                )

                var completeAddressString = Utils.getCompleteAddressString(
                    applicationContext,
                    point.latitude,
                    point.longitude
                ).replace("Unnamed Road,", "")
                val marker = MarkerOptions()
                    .position(LatLng(point.latitude, point.longitude))
                    .icon(markerIcon)
                    .title(
                        completeAddressString
                    )
                googleMap.addMarker(marker)

                if (address != null)
                    address.text = completeAddressString.trim()
                autoCompleteTextView.setText(completeAddressString)


                var apiInterface = RetrofitController.getClient().create(ApiInterfave::class.java)

                val call: Call<AlitudeResponse> =
                    apiInterface.getAltitude(point.latitude.toString() + "," + point.longitude.toString());
                call.enqueue(object : Callback<AlitudeResponse?> {
                    override fun onFailure(call: Call<AlitudeResponse?>?, t: Throwable?) {
                        Toast.makeText(this@LocationActivity, "Something went wrong!", Toast.LENGTH_SHORT)
                            .show()
                    }

                    override fun onResponse(
                        call: Call<AlitudeResponse?>?,
                        response: Response<AlitudeResponse?>?
                    ) {
                        if (response != null && response.isSuccessful) {
                            var altitudeResponse = response.body() as AlitudeResponse

                            if (altitudeResponse.status.equals("success")) {
                                var altitudeList = altitudeResponse.data
                                try {
                                    textView.text = altitudeList.get(0).toString() + " m"
                                }catch (e:NullPointerException){
                                    e.printStackTrace()
                                    textView.text ="Tap within radius"
                                }
                            }

                        }

                    }

                })
            }
        }

    }

}