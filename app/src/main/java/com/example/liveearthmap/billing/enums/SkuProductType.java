package com.example.liveearthmap.billing.enums;

public enum SkuProductType {
    CONSUMABLE,
    NON_CONSUMABLE,
    SUBSCRIPTION;
}
