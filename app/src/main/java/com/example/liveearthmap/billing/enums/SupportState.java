package com.example.liveearthmap.billing.enums;

public enum SupportState {
    SUPPORTED,
    NOT_SUPPORTED,
    DISCONNECTED
}