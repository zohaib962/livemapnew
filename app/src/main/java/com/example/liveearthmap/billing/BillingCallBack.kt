package com.example.liveearthmap.billing

interface BillingCallBack {
    fun onBillingPurchases()
    fun onBillingError()


}