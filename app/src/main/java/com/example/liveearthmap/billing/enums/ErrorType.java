package com.example.liveearthmap.billing.enums;

public enum ErrorType {
    CLIENT_NOT_READY,
    CLIENT_DISCONNECTED,
    ITEM_NOT_EXIST,
    ITEM_ALREADY_OWNED,
    CONSUME_ERROR,
    ACKNOWLEDGE_ERROR,
    FETCH_PURCHASED_PRODUCTS_ERROR,
    BILLING_ERROR;
}