package com.example.liveearthmap.billing

import android.app.Activity
import android.content.Context
import android.util.Log
import com.example.liveearthmap.R
import com.example.liveearthmap.billing.enums.ErrorType
import com.example.liveearthmap.billing.models.BillingResponse
import com.example.liveearthmap.billing.models.PurchaseInfo
import com.example.liveearthmap.billing.models.SkuInfo
import com.example.liveearthmap.other.SharePrefData
import java.util.*

class InAppBillingUtils {

    companion object {
        var billingConnector: BillingConnector? = null
        private var context: Context? = null
        private var mActivity: Activity? = null

        fun setData(context: Context) {
            Companion.context = context
            initializeBillingClient()
        }

        private fun initializeBillingClient() {
            val nonConsumableIds: MutableList<String> = ArrayList()
            nonConsumableIds.add(context?.getString(R.string.product_key)!!)
//            nonConsumableIds.add(context?.getString(R.string.sku_id)!!)
            billingConnector = BillingConnector(
                context,
                context?.getString(R.string.billing_id)
            ) //"license_key" - public developer key from Play Console
//                .setConsumableIds(nonConsumableIds) //to set consumable ids - call only for consumable products
                .setNonConsumableIds(nonConsumableIds) //to set non-consumable ids - call only for non-consumable products
//                .setSubscriptionIds(nonConsumableIds) //to set subscription ids - call only for subscription products
                .autoAcknowledge() //legacy option - better call this. Alternatively purchases can be acknowledge via public method "acknowledgePurchase(PurchaseInfo purchaseInfo)"
                .autoConsume() //legacy option - better call this. Alternatively purchases can be consumed via public method consumePurchase(PurchaseInfo purchaseInfo)"
                .enableLogging() //to enable logging for debugging throughout the library - this can be skipped
                .connect() //to connect billing client with Play Console
            billingConnector?.setBillingEventListener(object : BillingEventListener {
                override fun onProductsFetched(skuDetails: List<SkuInfo>) {
                }

                override fun onPurchasedProductsFetched(purchases: List<PurchaseInfo>) {
                    var purchase: String
                    for (purchaseInfo in purchases) {
                        purchase = purchaseInfo.skuId
                        if (purchase.equals(
                                context?.getString(R.string.product_key),
                                ignoreCase = true
                            )
                        ) {
                            SharePrefData.getInstance().adS_PREFS=true
                        }
                    }
                }

                override fun onProductsPurchased(purchases: List<PurchaseInfo>) {

                }

                override fun onPurchaseAcknowledged(purchase: PurchaseInfo) {
                }

                override fun onPurchaseConsumed(purchase: PurchaseInfo) {
                }

                override fun onBillingError(
                    billingConnector: BillingConnector,
                    response: BillingResponse
                ) {
                    when (response.errorType) {
                        ErrorType.CLIENT_NOT_READY -> {
                            Log.e("In-App Error", "Client not ready")
                        }
                        ErrorType.CLIENT_DISCONNECTED -> {
                            Log.e("In-App Error", "Client disconnected")
                        }
                        ErrorType.ITEM_NOT_EXIST -> {
                            Log.e("In-App Error", "item does not exist")
                        }
                        ErrorType.ITEM_ALREADY_OWNED -> {
                            Log.e("In-App Error", "item already owned")
                        }
                        ErrorType.ACKNOWLEDGE_ERROR -> {
                            Log.e("In-App Error", "acknowledge error")
                        }
                        ErrorType.CONSUME_ERROR -> {
                            Log.e("In-App Error", "consume error")
                        }
                        ErrorType.FETCH_PURCHASED_PRODUCTS_ERROR -> {
                            Log.e("In-App Error", "fetch error")
                        }
                        ErrorType.BILLING_ERROR -> {
                            Log.e("In-App Error", "billing error")
                        }
                    }
                    SharePrefData.getInstance().adS_PREFS=false
                }
            })
        }

        fun purchase(activity: Activity, callBack: BillingCallBack) {
            mActivity = activity


            billingConnector?.setBillingEventListener(object : BillingEventListener {
                override fun onProductsFetched(skuDetails: List<SkuInfo>) {
//                    var sku: String
//
//                    for (skuInfo in skuDetails) {
//                        sku = skuInfo.skuId
//                        if (sku.equals(context?.getString(R.string.sku_id), ignoreCase = true)) {
//                            Toast.makeText(
//                                context,
//                                "Product fetched: $sku", Toast.LENGTH_SHORT
//                            ).show()
//
                }

//                    fetchedSkuInfoList.add(skuInfo) //check "usefulPublicMethods" to see what's going on with this list
//                    }


                override fun onPurchasedProductsFetched(purchases: List<PurchaseInfo>) {

                }

                override fun onProductsPurchased(purchases: List<PurchaseInfo>) {
                    var purchase: String
                    for (purchaseInfo in purchases) {
                        purchase = purchaseInfo.skuId
                        if (purchase.equals(
                                context?.getString(R.string.product_key),
                                ignoreCase = true
                            )
                        ) {

//                            Toast.makeText(
//                                context,
//                                "Product purchased: $purchase", Toast.LENGTH_SHORT
//                            ).show()
                            Log.e("purchase","Purchased")
                            SharePrefData.getInstance().adS_PREFS=true
                            activity.runOnUiThread {
                                callBack.onBillingPurchases()
                            }

                        }
                    }
//                purchasedInfoList.addAll(purchases) //check "usefulPublicMethods" to see what's going on with this list
                }

                override fun onPurchaseAcknowledged(purchase: PurchaseInfo) {
                }

                override fun onPurchaseConsumed(purchase: PurchaseInfo) {
                }

                override fun onBillingError(
                    billingConnector: BillingConnector,
                    response: BillingResponse
                ) {
                    when (response.errorType) {
                        ErrorType.CLIENT_NOT_READY -> {
                            Log.e("In-App Error", "Client not ready")
                        }
                        ErrorType.CLIENT_DISCONNECTED -> {
                            Log.e("In-App Error", "Client disconnected")
                        }
                        ErrorType.ITEM_NOT_EXIST -> {
                            Log.e("In-App Error", "item does not exist")
                        }
                        ErrorType.ITEM_ALREADY_OWNED -> {
                            Log.e("In-App Error", "item already owned")
                        }
                        ErrorType.ACKNOWLEDGE_ERROR -> {
                            Log.e("In-App Error", "acknowledge error")
                        }
                        ErrorType.CONSUME_ERROR -> {
                            Log.e("In-App Error", "consume error")
                        }
                        ErrorType.FETCH_PURCHASED_PRODUCTS_ERROR -> {
                            Log.e("In-App Error", "fetch error")
                        }
                        ErrorType.BILLING_ERROR -> {
                            Log.e("In-App Error", "billing error")
                        }
                    }
                    SharePrefData.getInstance().adS_PREFS=false
                    activity.runOnUiThread {
                        callBack.onBillingError()
                    }

                }
            })
            billingConnector!!.purchase(activity, context?.getString(R.string.product_key))
        }
    }
}