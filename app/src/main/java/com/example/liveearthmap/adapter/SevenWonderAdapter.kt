package com.example.liveearthmap.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.liveearthmap.R
import com.example.liveearthmap.WonderDetailActivity
import com.example.liveearthmap.model.WonderData

class SevenWonderAdapter(val mData: List<WonderData>) :
    RecyclerView.Adapter<SevenWonderAdapter.MyViewHolder>() {


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title: TextView = itemView.findViewById(R.id.title)
        val image: ImageView = itemView.findViewById(R.id.image);

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_seven_wonder_single_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.title.text = mData[position].name
        holder.image.setImageResource(mData[position].icon)

        holder.image.setOnClickListener {
            holder.itemView.context.startActivity(
                Intent(
                    holder.itemView.context,
                    WonderDetailActivity::class.java
                ).putExtra("data", mData[position])
            )
        }
    }


    override fun getItemCount(): Int {
        return mData.size
    }


}