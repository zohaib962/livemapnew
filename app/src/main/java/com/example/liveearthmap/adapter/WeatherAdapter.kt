package com.example.liveearthmap.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.liveearthmap.R
import com.example.liveearthmap.model.WeatherHourly
import com.example.liveearthmap.other.Utils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_weather.*
import java.text.DateFormat
import java.text.SimpleDateFormat

class WeatherAdapter(val mData: List<WeatherHourly.AllList>) :
    RecyclerView.Adapter<WeatherAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val temp: TextView = itemView.findViewById(R.id.temp)
        val day: TextView = itemView.findViewById(R.id.day)
        val image: ImageView = itemView.findViewById(R.id.image)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.weather_single_row, parent, false)
        )
    }

    var current: String = ""
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.temp.text = (mData[position].main.temp - 273.15).toInt().toString() + "°"

        if (mData[position].weather.size > 0) {
            if (mData[position].weather[0].icon != "") {
                holder.image.setImageResource(Utils.getIcon(mData[position].weather[0].icon))
            }
        }

        holder.day.text = getDate(mData[position].dt_txt)

    }

    private fun getDate(t: String): String {

        val format1 = SimpleDateFormat("yyyy-dd-MM")
        val dt1 = format1.parse(t)
        val format2: DateFormat = SimpleDateFormat("EEEE")
        return format2.format(dt1)

    }

    override fun getItemCount(): Int {
        return mData.size
    }


}