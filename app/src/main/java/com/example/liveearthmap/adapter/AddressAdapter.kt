package com.example.liveearthmap.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.liveearthmap.OnSaveLocationItemClickListener
import com.example.liveearthmap.R
import com.example.liveearthmap.model.AddressDetail

class AddressAdapter(private val mData: ArrayList<AddressDetail>) :
    RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {


    lateinit var onSaveLocationItemClickListener: OnSaveLocationItemClickListener


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val name: TextView = itemView.findViewById(R.id.name)
        val address: TextView = itemView.findViewById(R.id.address)
        val copyAddress: View = itemView.findViewById(R.id.copyAddress)
        val shareAddress: View = itemView.findViewById(R.id.shareAddress)
        val deleteAddress: View = itemView.findViewById(R.id.deleteAddress)


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_saved_address_list_single_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.name.text = mData[position].name
        holder.address.text = mData[position].address

        holder.copyAddress.setOnClickListener {
            onSaveLocationItemClickListener.onCopy(mData[position])
        }
        holder.shareAddress.setOnClickListener {
            onSaveLocationItemClickListener.onShare(mData[position])
        }
        holder.itemView.setOnClickListener {
            onSaveLocationItemClickListener.onClick(mData[position])
        }
        holder.deleteAddress.setOnClickListener {
            onSaveLocationItemClickListener.onDelete(mData[position], position)
        }

    }


    override fun getItemCount(): Int {
        return mData.size
    }

    fun removeFromList(addressDetail: AddressDetail) {

        val indexOf = mData.indexOf(addressDetail)
        if (indexOf != -1) {
            mData.removeAt(indexOf)
            notifyDataSetChanged()

        } else
            for (i in 0..mData.size) {
                if (mData[i].id == addressDetail.id) {
                    mData.removeAt(i)
                    notifyDataSetChanged()
                    break
                }
            }
    }


}