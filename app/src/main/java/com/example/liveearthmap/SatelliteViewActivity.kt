package com.example.liveearthmap

import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.example.liveearthmap.other.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import kotlinx.android.synthetic.main.activity_sattlite_view.*
import kotlinx.android.synthetic.main.activity_sattlite_view.bannerad
import kotlinx.android.synthetic.main.activity_sattlite_view.root
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.search_location.*


class SatelliteViewActivity : LocationActivity(), OnMapReadyCallback {

    override fun updateLocation(location: Location?) {
    }

    override fun updateLocationAltidtude(location: LatLng?) {

    }

    var isNightMode = false
    var mapType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sattlite_view)

        if (!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
        }
        Utils.showMapToast(this)

        titleText.text = "Satellite View"


        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        toolbarImage.setImageResource(R.drawable.ic_back)
        toolbarImage.setOnClickListener {

            onBackPressed()
        }

        if (saveValue.isDarkTheme) {
            titleText.setTextColor(Color.WHITE)
            toolbarImage.setColorFilter(Color.WHITE)
            root.setBackgroundColor((Color.parseColor("#000230")))
        } else {
            root.setBackgroundColor(Color.WHITE)
        }

        currentLocation.setOnClickListener {
            goToCurrentLocation()
        }

        zoomIn.setOnClickListener {
            if (mMap != null) {
                mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
            }
        }

        zoomOut.setOnClickListener {
            if (mMap != null) {
                mMap!!.animateCamera(CameraUpdateFactory.zoomOut());
            }

        }



        autoCompleteTextView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                search(p0.toString())

            }

            override fun afterTextChanged(p0: Editable?) {

            }


        })


    }


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        googleMap.mapType = GoogleMap.MAP_TYPE_SATELLITE

        setOnMapClick(googleMap)
    }

    override fun onBackPressed() {

        if (RemoteKeys.lem_satellite_exit_inter_new.contains(Constants.AM, true)) {
            AdmobInterstitialAds.showAdmobInterstitial(
                this,
                RemoteKeys.lem_satellite_exit_inter_new,
                object : AdmobInterstitialAds.InterstitialAdsCallBack {
                    override fun onResult(showAppLovin: Boolean) {
                        if (RemoteKeys.lem_satellite_exit_inter_new == Constants.AM_AL && showAppLovin) {
                            InterstitialAdsAppLovin.showInterstitial(
                                this@SatelliteViewActivity,
                                RemoteKeys.lem_satellite_exit_inter_new,
                                object : InterstitialAppLovinCallback {
                                    override fun onResult() {
                                        finish()
                                    }

                                })
                        } else {
                            finish()
                        }
                    }
                })
        } else if (RemoteKeys.lem_satellite_exit_inter_new == Constants.AL) {
            InterstitialAdsAppLovin.showInterstitial(
                this,
                RemoteKeys.lem_satellite_exit_inter_new,
                object : InterstitialAppLovinCallback {
                    override fun onResult() {
                        finish()
                    }

                })
        } else{
            finish()
        }


    }

    fun setNightMode(view: View) {

        if (!isNightMode) {
            isNightMode = true
            val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.night_map_style)
            mMap?.setMapStyle(style)
            mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL


        } else {
            isNightMode = false

            val style = MapStyleOptions.loadRawResourceStyle(this, R.raw.default_map_style)
            mMap?.setMapStyle(style)
            mMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE


        }


    }

    fun setMapType(view: View) {

        when (mapType) {
            0 -> {
                mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                mapType = 1
            }
            1 -> {
                mMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
                mapType = 2
            }
            2 -> {
                mMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
                mapType = 0
            }
        }


    }


    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner_map == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

}