package com.example.liveearthmap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxAdView;
import com.example.liveearthmap.model.Stopwatch;
import com.example.liveearthmap.model.Utils;
import com.example.liveearthmap.other.AdmobInterstitialAds;
import com.example.liveearthmap.other.Constants;
import com.example.liveearthmap.other.InterstitialAdsAppLovin;
import com.example.liveearthmap.other.InterstitialAppLovinCallback;
import com.example.liveearthmap.other.MyLocation;
import com.example.liveearthmap.other.RemoteKeys;
import com.example.liveearthmap.other.SharePrefData;
import com.github.anastr.speedviewlib.AwesomeSpeedometer;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.common.ConnectionResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SpeedometerActivity extends BaseActivity implements Stopwatch.StopWatchListener, MyLocation.OnLocationUpdate,
        MaxAdViewAdListener {

    static SimpleDateFormat current = new SimpleDateFormat("hh:mm:ss aa");

    static SimpleDateFormat aveTime = new SimpleDateFormat("hh:mm:ss");


    MyLocation myLocation;
    boolean isRunning;
    int lastSpeed = -1;
    Location currentLocation;
    Location lastLocation = null;
    Stopwatch stopwatch;
    private Context context;


    private TextView totalDistance;
    private AwesomeSpeedometer pointerSpeedometer;
    private TextView maxSpeed;
    private TextView speedTextView;
    private TextView speedTextViewBlur;
    private TextView averageSpeed;
    private TextView textView;
    private TextView titleText;
    private ImageView toolbarImage;
    private View root;


    boolean isPlaying;

    FrameLayout admobNativeView;
    View adlayout;

    private MaxAdView adView;
    FrameLayout rootView;

    private void loadAppLovinBannerAd(){
        adView = new MaxAdView( SharePrefData.getInstance().getApplovinBannerr(), this );
        adView.setListener( this );
        // Stretch to the width of the screen for banners to be fully functional
        int width = ViewGroup.LayoutParams.MATCH_PARENT;

        // Banner height on phones and tablets is 50 and 90, respectively
        int heightPx = getResources().getDimensionPixelSize( R.dimen.banner_height );

        adView.setLayoutParams( new FrameLayout.LayoutParams( width, heightPx ) );

        // Set background or background color for banners to be fully functional
        adView.setBackgroundColor(ContextCompat.getColor(this,R.color.white));


        rootView.addView( adView );

        // Load the ad
        adView.loadAd();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speedometer);
        rootView = findViewById(R.id.bannerad);
        if(RemoteKeys.INSTANCE.getLem_applovin_banner().equals(Constants.INSTANCE.getAL()) && !SharePrefData.getInstance().getApplovinBannerr().isEmpty())
        {loadAppLovinBannerAd();}
        context = this;
        admobNativeView = findViewById(R.id.admobNativeView);
        adlayout = findViewById(R.id.adlayout);
        titleText = findViewById(R.id.titleText);

        toolbarImage = findViewById(R.id.toolbarImage);
        titleText.setText("Speedometer");
        root = findViewById(R.id.root);
        titleText.setTextColor(Color.WHITE);
        toolbarImage.setColorFilter(Color.WHITE);
        toolbarImage.setImageResource(R.drawable.ic_back);
        toolbarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

     /*   if (saveValue.isDarkTheme()) {
            root.setBackgroundColor(Color.parseColor("#000230"));
        } else {
            root.setBackgroundColor(Color.WHITE);
        }*/


        stopwatch = new Stopwatch(this);

        getLocation();

        // UI bind
        speedTextViewBlur = findViewById(R.id.speedTextViewBlur);
        speedTextView = findViewById(R.id.speedTextView);
        maxSpeed = findViewById(R.id.maxSpeed);
        averageSpeed = findViewById(R.id.averageSpeed);
        pointerSpeedometer = findViewById(R.id.pointerSpeedometer);

        totalDistance = findViewById(R.id.totalDistance);


        textView = findViewById(R.id.play);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPlaying) {
                    stopRide();
                    isPlaying = false;
                    textView.setText("Play");
                } else {
                    playRide();
                    isPlaying = true;
                    textView.setText("Stop");
                }


                /*if (rideStatus == RideStatus.STOP) {
                    stopRide();

                    // next step is play
                    rideStatus = RideStatus.PLAY;

                } else if (rideStatus == RideStatus.PLAY) {
                    playRide();

//                    next step is resume
                    rideStatus = RideStatus.PAUSE;

                } else if (rideStatus == RideStatus.RESUME) {
                    stopwatch.resume();
                    if (myLocation != null)
                        myLocation.start();
                } else if (rideStatus == RideStatus.PAUSE) {
                    stopwatch.pause();
                    if (myLocation != null)
                        myLocation.stop();
                }
*/


            }
        });


//        if (SharePrefData.getInstance().getIs_admob_splash().equals("true") && !SharePrefData.getInstance().getADS_PREFS()) {
//            loadAdmobNativeAd();
//        } else if (SharePrefData.getInstance().getIs_admob_splash().equals("false") && !SharePrefData.getInstance().getADS_PREFS()) {
//            loadNativeAd();
//        } else {
//            admobNativeView.setVisibility(View.GONE);
//            nativeAdContainer.setVisibility(View.GONE);
//            adlayout.setVisibility(View.GONE);
//        }

    }

    @Override
    public void onBackPressed() {

        if (RemoteKeys.INSTANCE.getLem_speed_exit_inter_new().contains(Constants.INSTANCE.getAM())) {
            AdmobInterstitialAds.INSTANCE.showAdmobInterstitial(
                    this,
                    RemoteKeys.INSTANCE.getLem_speed_exit_inter_new(),
                    new AdmobInterstitialAds.InterstitialAdsCallBack() {
                        @Override
                        public void onResult(boolean showAppLovin) {
                            if (RemoteKeys.INSTANCE.getLem_speed_exit_inter_new().equals(Constants.INSTANCE.getAM_AL()) && showAppLovin) {
                                InterstitialAdsAppLovin.INSTANCE.showInterstitial(
                                        SpeedometerActivity.this,
                                        RemoteKeys.INSTANCE.getLem_speed_exit_inter_new(),
                                        new InterstitialAppLovinCallback() {
                                            @Override
                                            public void onResult() {
                                                finish();
                                            }
                                        });
                            } else {
                                finish();
                            }
                        }
                    });
        } else if (RemoteKeys.INSTANCE.getLem_speed_exit_inter_new().equals(Constants.INSTANCE.getAL())) {
            InterstitialAdsAppLovin.INSTANCE.showInterstitial(
                    SpeedometerActivity.this,
                    RemoteKeys.INSTANCE.getLem_speed_exit_inter_new(),
                    new InterstitialAppLovinCallback() {
                        @Override
                        public void onResult() {
                            finish();
                        }
                    });
        } else{
            finish();
        }


    }

    @Override
    public void onTick(String time) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;

    }


    private void playRide() {
        if (myLocation != null) {
            myLocation.start();
            stopwatch.restart();
            stopwatch.start();
            currentLocation = null;

            lastSpeed = -1;
        }

    }

    private void stopRide() {


        stopwatch.stop();
        if (myLocation != null)
            myLocation.stop();
        pointerSpeedometer.speedTo(0, 1000);


    }


    private void getLocation() {
        if (Utils.checkLocationPermission(context)) {
            myLocation = new MyLocation(context, this,this);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.INSTANCE.getREQUEST_CHECK_SETTINGS()) {

            if (Utils.checkLocationPermission(context)) {
                myLocation = new MyLocation(context, this,this);
                myLocation.start();
            }

        }
    }

    @Override
    public void locationChange(Location location) {

        currentLocation = location;


        if (location != null && isRunning) {

            if (lastLocation == null)
                lastLocation = location;


            int speed = (int) (location.getSpeed() * 3.6);

            if (speed == 0) {
                pointerSpeedometer.speedTo(0);
                return;
            }

            pointerSpeedometer.speedTo(speed);

            speedTextView.setText("" + ((int) speed));

            if (speed >= 100) {
                speedTextViewBlur.setText("" + ((int) speed));
            } else {
                speedTextViewBlur.setText("0" + ((int) speed));
            }


            if (speed > lastSpeed) {
                lastSpeed = speed;
                maxSpeed.setText(lastSpeed + "Km/h");

            }


            float distance = lastLocation.distanceTo(location);


            try {

                float totalTimeInHour = ((stopwatch.getElapsedTimeHour() * 3600)
                        + (stopwatch.getElapsedTimeMin() * 60) + stopwatch.getElapsedTimeSecs())
                        / 3600f;

                float distanceInKM = (distance / 1000);
                int avgInKM = (int) (distanceInKM / totalTimeInHour);
                averageSpeed.setText(avgInKM + "Km/h");


            } catch (Exception e) {

            }


            if (distance <= 1000.0) {
                totalDistance.setText(String.format("%.2f", distance) + " " + getString(R.string.m));

            } else {
                distance /= 1000.0;
                totalDistance.setText(String.format("%.2f", distance) + " " + getString(R.string.km));
            }

        }

    }

    @Override
    public void connectionSuspended(int i) {

    }

    @Override
    public void connectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        myLocation.stop();
    }


    @Override
    public void onAdExpanded(MaxAd ad) {

    }

    @Override
    public void onAdCollapsed(MaxAd ad) {

    }

    @Override
    public void onAdLoaded(MaxAd ad) {

    }

    @Override
    public void onAdDisplayed(MaxAd ad) {

    }

    @Override
    public void onAdHidden(MaxAd ad) {

    }

    @Override
    public void onAdClicked(MaxAd ad) {

    }

    @Override
    public void onAdLoadFailed(String adUnitId, MaxError error) {
        rootView.setVisibility(View.GONE);
    }

    @Override
    public void onAdDisplayFailed(MaxAd ad, MaxError error) {
        rootView.setVisibility(View.GONE);
    }
}
