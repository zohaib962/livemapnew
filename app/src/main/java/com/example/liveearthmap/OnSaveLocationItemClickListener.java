package com.example.liveearthmap;

import com.example.liveearthmap.model.AddressDetail;

public interface OnSaveLocationItemClickListener {
    void onCopy(AddressDetail addressDetail);
    void onShare(AddressDetail addressDetail);
    void onDelete(AddressDetail addressDetail , int position);
    void onClick(AddressDetail addressDetail);
}
