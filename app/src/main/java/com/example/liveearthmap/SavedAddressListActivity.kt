package com.example.liveearthmap

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.example.liveearthmap.adapter.AddressAdapter
import com.example.liveearthmap.db.DB
import com.example.liveearthmap.db.TaskDao
import com.example.liveearthmap.model.AddressDetail
import com.example.liveearthmap.other.*
import kotlinx.android.synthetic.main.activity_saved_address_list.*
import kotlinx.android.synthetic.main.activity_saved_address_list.admobNativeView
import kotlinx.android.synthetic.main.activity_saved_address_list.bannerad
import kotlinx.android.synthetic.main.activity_saved_address_list.mainAdLayout
import kotlinx.android.synthetic.main.activity_saved_address_list.noAdView
import kotlinx.android.synthetic.main.activity_saved_address_list.root
import kotlinx.android.synthetic.main.custom_toolbar.*

class SavedAddressListActivity : BaseActivity(), OnSaveLocationItemClickListener {
    lateinit var addressAdapter: AddressAdapter
    lateinit var get: TaskDao;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saved_address_list)
        if (!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
        }

        titleText.text = "Save Location"
        toolbarImage.setImageResource(R.drawable.ic_back)
        toolbarImage.setOnClickListener {
           onBackPressed()
        }

        titleText.setTextColor(Color.WHITE)
        toolbarImage.setColorFilter(Color.WHITE)
        if (saveValue.isDarkTheme) {
            root.setBackgroundColor((Color.parseColor("#000230")))
        } else {
            root.setBackgroundResource(R.drawable.save_location_bg_main)
        }


        recyclerViewe.layoutManager = LinearLayoutManager(this)
        get = DB.get(this)

        val list = ArrayList<AddressDetail>()
        list.addAll(get.allAddress)

        addressAdapter = AddressAdapter(list)
        recyclerViewe.adapter = addressAdapter
        addressAdapter.onSaveLocationItemClickListener = this


        NativeAdsApplovin.showNativeAd(this,this,RemoteKeys.lem_saveaddress_native_new,admobNativeView,object :
            NativeAppLovinCallback {
            override fun onLoaded() {
                mainAdLayout.visibility=View.VISIBLE
            }

            override fun onFailed() {
                mainAdLayout.visibility=View.GONE
            }

        })


        noAdView.setOnClickListener {
            startActivity(Intent(this,AdRemoveActivity::class.java))
        }


    }

    override fun onClick(addressDetail: AddressDetail?) {
        startActivity(
            Intent(
                applicationContext,
                ShareAddressActivity::class.java
            ).putExtra("address" , addressDetail)
        )
    }


    override fun onCopy(addressDetail: AddressDetail?) {

        val clipboard: ClipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copy text", addressDetail!!.address)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(applicationContext, "Copy to clipboard", Toast.LENGTH_LONG).show()
    }

    override fun onDelete(addressDetail: AddressDetail?, position: Int) {
        AlertDialog.Builder(this)
            .setTitle("Delete")
            .setMessage("Are you sure to delete this address?")
            .setPositiveButton(
                "Delete"
            ) { dialog, whichButton ->
                get.deleteAddress(addressDetail!!.id)
                addressAdapter.removeFromList(addressDetail);
                Toast.makeText(applicationContext, "Address removed!", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
            }
            .setNegativeButton("Cancel", null).show()

    }


    override fun onShare(addressDetail: AddressDetail?) {
        val intent = Intent(Intent.ACTION_SEND)

        intent.type = "text/plain"
        intent.putExtra(
            Intent.EXTRA_SUBJECT,
            "Share using"
        )
        intent.putExtra(Intent.EXTRA_TEXT, addressDetail!!.address)
        startActivity(
            Intent.createChooser(
                intent, "Share using"
            )
        )
    }

    override fun onBackPressed() {

        if (RemoteKeys.lem_saveaddress_exit_inter_new.contains(Constants.AM, true)) {
            AdmobInterstitialAds.showAdmobInterstitial(
                this,
                RemoteKeys.lem_saveaddress_exit_inter_new,
                object : AdmobInterstitialAds.InterstitialAdsCallBack {
                    override fun onResult(showAppLovin: Boolean) {
                        if (RemoteKeys.lem_saveaddress_exit_inter_new == Constants.AM_AL && showAppLovin) {
                            InterstitialAdsAppLovin.showInterstitial(
                                this@SavedAddressListActivity,
                                RemoteKeys.lem_saveaddress_exit_inter_new,
                                object : InterstitialAppLovinCallback {
                                    override fun onResult() {
                                        finish()
                                    }

                                })
                        } else {
                            finish()
                        }
                    }
                })
        } else if (RemoteKeys.lem_saveaddress_exit_inter_new == Constants.AL) {
            InterstitialAdsAppLovin.showInterstitial(
                this,
                RemoteKeys.lem_saveaddress_exit_inter_new,
                object : InterstitialAppLovinCallback {
                    override fun onResult() {
                        finish()
                    }

                })
        } else{
            finish()
        }

    }


    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

}