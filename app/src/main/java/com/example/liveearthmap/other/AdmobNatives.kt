package com.example.liveearthmap.other

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.example.liveearthmap.R
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.mediation.MediationNativeAdCallback
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView

object AdmobNatives {


    var amNative: NativeAd? = null

    fun loadAdmobNative(
        context: Context,view: FrameLayout, adValue: String,nativeAdCallback: NativeAdCallback
    ,adId:String) {
        if(adValue.contains(Constants.AM,true) && !SharePrefData.getInstance().adS_PREFS) {
            val adLoader = AdLoader.Builder(
                context,
                adId
            ).forNativeAd { ad: NativeAd ->
                amNative = ad
            }.withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    amNative = null
                    nativeAdCallback.failed()
                    Log.e("AM_NATIVE", "Failed: " + adError.code + " | " + adError.message)
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    nativeAdCallback.success()
                    Log.e("AM_NATIVE", "Loaded")
                    inflateAmNative(context, amNative!!, view,adValue)
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    amNative = null
                }
            }).withNativeAdOptions(NativeAdOptions.Builder().build()).build()
            adLoader.loadAd(AdRequest.Builder().build())
        }else{
            nativeAdCallback.failed()
        }
    }

    fun inflateAmNative(context: Context, nativeAd: NativeAd, amLayout: FrameLayout,adValue:String) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                as LayoutInflater

        val adView = if(adValue==Constants.AM_LCTR) {
            inflater.inflate(R.layout.admob_native_layout_lctr2, null) as NativeAdView
        }else{
            inflater.inflate(R.layout.loading_admob_native, null) as NativeAdView
        }
        amLayout.removeAllViews()
        amLayout.addView(adView)

        adView.mediaView = adView.findViewById(R.id.adMediaView)

        adView.headlineView = adView.findViewById(R.id.adHeadlineTxt)
        adView.bodyView = adView.findViewById(R.id.txtbody)
        adView.callToActionView = adView.findViewById(R.id.btnClickAction)
        adView.iconView = adView.findViewById(R.id.adiconTxt)
        /*adView.priceView = adView.findViewById(R.id.ad_price)
        adView.starRatingView = adView.findViewById(R.id.ad_stars)
        adView.storeView = adView.findViewById(R.id.ad_store)
        adView.advertiserView = adView.findViewById(R.id.ad_advertiser)*/

        (adView.headlineView as TextView).text = nativeAd.headline
        adView.mediaView.setMediaContent(nativeAd.mediaContent)

        if (nativeAd.body == null) {
            adView.bodyView.visibility = View.INVISIBLE
        } else {
            adView.bodyView.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }

        if (nativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.INVISIBLE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as Button).text = nativeAd.callToAction
        }

        if (nativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }

       /* if (nativeAd.price == null) {
            adView.priceView.visibility = View.INVISIBLE
        } else {
            adView.priceView.visibility = View.VISIBLE
            (adView.priceView as TextView).text = nativeAd.price
        }

        if (nativeAd.store == null) {
            adView.storeView.visibility = View.INVISIBLE
        } else {
            adView.storeView.visibility = View.VISIBLE
            (adView.storeView as TextView).text = nativeAd.store
        }

        if (nativeAd.starRating == null) {
            adView.starRatingView.visibility = View.INVISIBLE
        } else {
            (adView.starRatingView as RatingBar).rating = nativeAd.starRating!!.toFloat()
            adView.starRatingView.visibility = View.VISIBLE
        }

        if (nativeAd.advertiser == null) {
            adView.advertiserView.visibility = View.INVISIBLE
        } else {
            (adView.advertiserView as TextView).text = nativeAd.advertiser
            adView.advertiserView.visibility = View.VISIBLE
        }*/
        adView.setNativeAd(nativeAd)
    }

    interface NativeAdCallback{
        fun success()
        fun failed()
    }


}