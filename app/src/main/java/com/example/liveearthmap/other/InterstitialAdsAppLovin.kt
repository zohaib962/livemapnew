package com.example.liveearthmap.other

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.CountDownTimer
import android.util.Log
import android.view.Window
import android.widget.TextView
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxInterstitialAd

@SuppressLint("LogNotTimber")
object InterstitialAdsAppLovin {

    private lateinit var mInterstitialAd: MaxInterstitialAd
    private lateinit var splashInterstitial: MaxInterstitialAd
    private lateinit var dialog : Dialog
    private var failedcheck = true


    fun loadInterstitialAd(activity: Activity) {

        if (!SharePrefData.getInstance().adS_PREFS && SharePrefData.getInstance().appLovinInter.toString()
                .isNotEmpty()){
            Log.d("applovin33", "loadInterstitialAd: ")
            mInterstitialAd = MaxInterstitialAd(SharePrefData.getInstance().appLovinInter.toString().trim(), activity)
            mInterstitialAd.loadAd()
        }

    }

    fun showInterstitial(activity: Activity, remoteKey: String, interAdDismissCallback: InterstitialAppLovinCallback) {
        Log.d("applovin33", "showInterstitial: ")

        if (this::mInterstitialAd.isInitialized){
            if (mInterstitialAd.isReady && remoteKey.contains(Constants.AL,true)
                && !SharePrefData.getInstance().adS_PREFS) {

                mInterstitialAd.showAd()


            }else{
                interAdDismissCallback.onResult()
            }
        }else{
            interAdDismissCallback.onResult()
            return
        }

        mInterstitialAd.setListener(object : MaxAdListener {
            override fun onAdLoaded(ad: MaxAd) {
                Log.d("applovin33", "showInterstitialonAdLoaded: ")
            }

            override fun onAdDisplayed(ad: MaxAd) {
                Log.d("applovin33", "showInterstitialonAdDisplayed: ")
            }

            override fun onAdHidden(ad: MaxAd) {
                Log.d("applovin33", "showInterstitialonAdHidden: ")
                loadInterstitialAd(activity)
                interAdDismissCallback.onResult()
            }

            override fun onAdClicked(ad: MaxAd) {}
            override fun onAdLoadFailed(adUnitId: String, error: MaxError) {
                Log.d("applovin33", "showInterstitialonAdLoadFailed: ")
                interAdDismissCallback.onResult()
            }

            override fun onAdDisplayFailed(ad: MaxAd, error: MaxError) {
                Log.d("applovin33", "showInterstitialonAdDisplayFailed: ")
                destroyAndReload()
                interAdDismissCallback.onResult()
            }
        })
    }


    fun destroyAndReload(){
        mInterstitialAd.destroy()
        mInterstitialAd.loadAd()
    }

    fun loadSplashInterstitial(activity: Activity, remoteKey: String){

        if (remoteKey.contains(Constants.AL,true) && !SharePrefData.getInstance().adS_PREFS && SharePrefData.getInstance().appLovinInter.toString()
                .isNotEmpty()){

            splashInterstitial = MaxInterstitialAd(SharePrefData.getInstance().appLovinInter.toString().trim(), activity)
            splashInterstitial.loadAd()
        }
    }

    fun showSplashInterstitial(activity: Activity, remoteKey: String, interAdDismissCallback: InterstitialAppLovinCallback) {
        Log.d("applovin33", "showInterstitial: ")

        if (this::splashInterstitial.isInitialized){
            if (splashInterstitial.isReady && remoteKey.contains(Constants.AL,true)
                && !SharePrefData.getInstance().adS_PREFS) {

                splashInterstitial.showAd()


            }else{
                interAdDismissCallback.onResult()
            }
        }else{
            interAdDismissCallback.onResult()
            return
        }

        splashInterstitial.setListener(object : MaxAdListener {
            override fun onAdLoaded(ad: MaxAd) {
                Log.d("applovin34", "showInterstitialonAdLoaded: ")
            }

            override fun onAdDisplayed(ad: MaxAd) {
                Log.d("applovin34", "showInterstitialonAdDisplayed: ")
            }

            override fun onAdHidden(ad: MaxAd) {
                Log.d("applovin34", "showInterstitialonAdHidden: ")
                interAdDismissCallback.onResult()
            }

            override fun onAdClicked(ad: MaxAd) {}
            override fun onAdLoadFailed(adUnitId: String, error: MaxError) {
                Log.d("applovin34", "showInterstitialonAdLoadFailed: ")
                interAdDismissCallback.onResult()
            }

            override fun onAdDisplayFailed(ad: MaxAd, error: MaxError) {
                Log.d("applovin34", "showInterstitialonAdDisplayFailed: ")
                destroyAndReload()
                interAdDismissCallback.onResult()
            }
        })
    }


}