package com.example.liveearthmap.other

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxError
import com.applovin.mediation.nativeAds.MaxNativeAdListener
import com.applovin.mediation.nativeAds.MaxNativeAdLoader
import com.applovin.mediation.nativeAds.MaxNativeAdView
import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder
import com.example.liveearthmap.R


@SuppressLint("LogNotTimber")
object NativeAdsApplovin {

    var innerNative: MaxNativeAdLoader? = null
    var splashNative: MaxNativeAdLoader? = null
    private var appLovinNativeAdView: MaxNativeAdView? = null
    private var appLovinNativeAd: MaxAd? = null
    private var failedCheck = true


    fun loadNativeAd(activity: Activity, context: Context) {

        if (!SharePrefData.getInstance().adS_PREFS && SharePrefData.getInstance().appLovinNative.toString()
                .isNotEmpty()){
            Log.d("applovin33", "loadNativeAd: ")
            innerNative = MaxNativeAdLoader(SharePrefData.getInstance().appLovinNative.toString().trim(), activity)
            innerNative!!.setNativeAdListener(object : MaxNativeAdListener() {
                override fun onNativeAdLoaded(nativeAdView: MaxNativeAdView, ad: MaxAd) {
                    Log.d("applovin33", "nativeAd onNativeAdLoaded: ")

                    appLovinNativeAd = ad
                    appLovinNativeAdView = nativeAdView
                }

                override fun onNativeAdLoadFailed(adUnitId: String, error: MaxError) {
                    Log.d("applovin33", "nativeAd onNativeAdLoadFailed: ${error.code} : ${error.message}: ${error.waterfall}")
                    appLovinNativeAdView = null
                    appLovinNativeAd = null
                    if (failedCheck){
                        failedCheck = false
                        object: CountDownTimer(64000, 1000){
                            override fun onTick(p0: Long) {}
                            override fun onFinish() {
                                loadNativeAd(activity, context)
                            }

                        }.start()
                    }
                }

                override fun onNativeAdClicked(ad: MaxAd) {
                    Log.d("applovin33", "nativeAd onNativeAdClicked: ")
                }
            })
            innerNative!!.loadAd(createNativeAdView(context))
        }

    }


    fun showNativeAd(activity: Activity, context: Context,remoteKey: String, nativeAdContainer: FrameLayout, callback: NativeAppLovinCallback) {

        if (remoteKey.contains(Constants.AL) && appLovinNativeAdView != null && appLovinNativeAd != null && !SharePrefData.getInstance().adS_PREFS) {
            callback?.onLoaded()
            nativeAdContainer.visibility = View.VISIBLE
            if (appLovinNativeAdView!!.parent != null) {
                (appLovinNativeAdView!!.parent as ViewGroup).removeView(appLovinNativeAdView) // <- fix
            }
            nativeAdContainer.removeAllViews()
            nativeAdContainer.addView(appLovinNativeAdView)
            loadNativeAd(activity, context)
        }else{
            callback?.onFailed()
        }
    }

    fun splashNativeAds(activity: Activity, context: Context ,frameLayout: FrameLayout, callback: NativeAppLovinCallback?){
        if (!SharePrefData.getInstance().adS_PREFS && SharePrefData.getInstance().appLovinNative.toString()
                .isNotEmpty()){
            Log.d("applovin33", "loadNativeAd: ")

            frameLayout.visibility = View.GONE

            splashNative = MaxNativeAdLoader(SharePrefData.getInstance().appLovinNative.toString().trim(), activity)
            splashNative!!.setNativeAdListener(object : MaxNativeAdListener() {
                override fun onNativeAdLoaded(nativeAdView: MaxNativeAdView, ad: MaxAd) {
                    Log.d("applovin33", "nativeAd onNativeAdLoaded: ")
                    callback?.onLoaded()
                    frameLayout.visibility = View.VISIBLE
                    frameLayout.removeAllViews()
                    frameLayout.addView(nativeAdView)
                }

                override fun onNativeAdLoadFailed(adUnitId: String, error: MaxError) {
                    Log.d("applovin33", "nativeAd onNativeAdLoadFailed: ")
                    frameLayout.visibility = View.GONE
                    callback?.onFailed()
                }

                override fun onNativeAdClicked(ad: MaxAd) {
                    Log.d("applovin33", "nativeAd onNativeAdClicked: ")
                }
            })
            splashNative!!.loadAd(createNativeAdView(context))
        }else{
            frameLayout.visibility = View.GONE
        }

    }

    private fun createNativeAdView(context: Context): MaxNativeAdView {
        val binder: MaxNativeAdViewBinder =
            MaxNativeAdViewBinder.Builder(R.layout.custom_native_applovin)
                .setTitleTextViewId(R.id.title_text_view)
                .setBodyTextViewId(R.id.body_text_view)
                .setAdvertiserTextViewId(R.id.advertiser_textView)
                .setIconImageViewId(R.id.icon_image_view)
                .setMediaContentViewGroupId(R.id.media_view_container)
                .setOptionsContentViewGroupId(R.id.ad_options_view)
                .setCallToActionButtonId(R.id.cta_button)
                .build()
        return MaxNativeAdView(binder, context)
    }


}