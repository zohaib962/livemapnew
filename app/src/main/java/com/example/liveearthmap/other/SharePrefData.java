package com.example.liveearthmap.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharePrefData {
    private static final SharePrefData instance = new SharePrefData();

    private static final String ADS_PREFS = "adprefs", ADMOB_INTER="admobbanner", ADMOB_NATIVE="admobnative",
            privacy="privacy", ADMOB_INTER_INNER="admobinterinner", ADMOB_NATIVE_INNER="admobnativeinner"
            , APPLOVIN_INTER="aplovininter"   , APPLOVIN_NATIVE="aplovinnative", APPLOVIN_BANNER="aplovinbanner", APPLOVIN_RECTANGLE="aplovinrectangle"

    ;
    private SharedPreferences sp;
    private SharedPreferences.Editor spEditor;

    public static synchronized SharePrefData getInstance() {
        return instance;
    }
    private Context context;
    public void setContext(Context context) {
        this.sp = PreferenceManager.getDefaultSharedPreferences(context);
        this.context=context;
    }

    public SharePrefData(Context context) {
        this.sp = PreferenceManager.getDefaultSharedPreferences(context);
        this.context=context;
    }

    private SharePrefData() {
    }

    public Boolean getADS_PREFS() {
        return sp.getBoolean(ADS_PREFS, false);
    }

    public void setADS_PREFS(Boolean ADS_PREFS_) {
        if(sp==null){
            this.sp = PreferenceManager.getDefaultSharedPreferences(context);
        }
        spEditor = sp.edit();
        spEditor.putBoolean(ADS_PREFS, ADS_PREFS_);
        spEditor.apply();
        spEditor.commit();
    }

    public Boolean getPrivacy() {
        return sp.getBoolean(privacy, false);
    }

    public void setPrivacy(Boolean privacy_) {
        spEditor = sp.edit();
        spEditor.putBoolean(privacy, privacy_);
        spEditor.apply();
        spEditor.commit();
    }


    @SuppressLint("CommitPrefEdits")
    public boolean destroyUserSession() {
        this.spEditor = this.sp.edit();
        this.spEditor.apply();
        return true;
    }


    public void setApplovinRectangle(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(APPLOVIN_RECTANGLE, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getApplovinRectangle() {
        return sp.getString(APPLOVIN_RECTANGLE, "abc");
    }

    public void setAppLovinBannerr(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(APPLOVIN_BANNER, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getApplovinBannerr() {
        return sp.getString(APPLOVIN_BANNER, "abc");
    }

    public void setAppLovinNative(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(APPLOVIN_NATIVE, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getAppLovinNative() {
        return sp.getString(APPLOVIN_NATIVE, "abc");
    }

    public void setAppLovinInter(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(APPLOVIN_INTER, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getAppLovinInter() {
        return sp.getString(APPLOVIN_INTER, "abc");
    }

    public void setAdmobInter(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(ADMOB_INTER, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getAdmobInter() {
        return sp.getString(ADMOB_INTER, "abc");
    }


    public void setAdmobNative(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(ADMOB_NATIVE, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getAdmobNative() {
        return sp.getString(ADMOB_NATIVE, "abc");
    }

    //

    public void setAdmobInterInner(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(ADMOB_INTER_INNER, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getAdmobInterInner() {
        return sp.getString(ADMOB_INTER_INNER, "abc");
    }

    public void setAdmobNativeInner(String isadmob) {
        spEditor = sp.edit();
        spEditor.putString(ADMOB_NATIVE_INNER, isadmob);
        spEditor.apply();
        spEditor.commit();
    }

    public String getAdmobNativeInner() {
        return sp.getString(ADMOB_NATIVE_INNER, "abc");
    }

}
