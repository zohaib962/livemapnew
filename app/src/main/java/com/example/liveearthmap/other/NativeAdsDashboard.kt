package com.example.liveearthmap.other

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.example.liveearthmap.R
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView

object NativeAdsDashboard {

    var amNative: NativeAd? = null
    var amNativeSplash: NativeAd? = null
    var adFailCounter = true
    var isAdClicked = false

    fun loadAd(context: Context) {

        if (!SharePrefData.getInstance().adS_PREFS){

            val adLoader = AdLoader.Builder(context,SharePrefData.getInstance().admobNativeInner.toString().trim()).forNativeAd { ad: NativeAd ->
                amNative = ad
            }.withAdListener(object : AdListener() {

                override fun onAdFailedToLoad(adError: LoadAdError) {
                    amNative = null
//                    Log.e("AM_NATIVE", "Failed: " + adError.code + " | " + adError.message)
                    if (adFailCounter){
                        adFailCounter = false
                        loadAd(context)
                    }
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
//                    Log.e("AM_NATIVE", "Loaded dash")
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    isAdClicked = true
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    amNative = null
                    loadAd(context)
                }
            }).withNativeAdOptions(NativeAdOptions.Builder().build()).build()
            adLoader.loadAd(AdRequest.Builder().build())
        }
    }

    fun showAd(
        context: Context, remoteKeys: String, amLayout: FrameLayout,
        nativeAdsCallBack: NativeAdsCallBack
    ) {

        if (amNative != null && remoteKeys.contains(Constants.AM) && !SharePrefData.getInstance().adS_PREFS) {
            nativeAdsCallBack?.onAdLoaded()
            amLayout.visibility = View.VISIBLE

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val adView = if(remoteKeys==Constants.AM_LCTR) {
                inflater.inflate(R.layout.admob_native_layout_lctr2, null) as NativeAdView
            }else{
                inflater.inflate(R.layout.loading_admob_native, null) as NativeAdView
            }

            amLayout.removeAllViews()
            amLayout.addView(adView)

            adView.mediaView = adView.findViewById(R.id.adMediaView)

            adView.headlineView = adView.findViewById(R.id.adHeadlineTxt)
            adView.bodyView = adView.findViewById(R.id.txtbody)
            adView.callToActionView = adView.findViewById(R.id.btnClickAction)
            adView.iconView = adView.findViewById(R.id.adiconTxt)
            /*adView.priceView = adView.findViewById(R.id.ad_price)
            adView.starRatingView = adView.findViewById(R.id.ad_stars)
            adView.storeView = adView.findViewById(R.id.ad_store)
            adView.advertiserView = adView.findViewById(R.id.ad_advertiser)*/

            (adView.headlineView as TextView).text = amNative!!.headline
            adView.mediaView.setMediaContent(amNative!!.mediaContent)

            if (amNative!!.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.bodyView.visibility = View.VISIBLE
                (adView.bodyView as TextView).text = amNative!!.body
            }

            if (amNative!!.callToAction == null) {
                adView.callToActionView.visibility = View.INVISIBLE
            } else {
                adView.callToActionView.visibility = View.VISIBLE
                (adView.callToActionView as Button).text = amNative!!.callToAction
            }

            if (amNative!!.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                (adView.iconView as ImageView).setImageDrawable(
                    amNative!!.icon.drawable
                )
                adView.iconView.visibility = View.VISIBLE
            }

            /* if (nativeAd.price == null) {
                 adView.priceView.visibility = View.INVISIBLE
             } else {
                 adView.priceView.visibility = View.VISIBLE
                 (adView.priceView as TextView).text = nativeAd.price
             }

             if (nativeAd.store == null) {
                 adView.storeView.visibility = View.INVISIBLE
             } else {
                 adView.storeView.visibility = View.VISIBLE
                 (adView.storeView as TextView).text = nativeAd.store
             }

             if (nativeAd.starRating == null) {
                 adView.starRatingView.visibility = View.INVISIBLE
             } else {
                 (adView.starRatingView as RatingBar).rating = nativeAd.starRating!!.toFloat()
                 adView.starRatingView.visibility = View.VISIBLE
             }

             if (nativeAd.advertiser == null) {
                 adView.advertiserView.visibility = View.INVISIBLE
             } else {
                 (adView.advertiserView as TextView).text = nativeAd.advertiser
                 adView.advertiserView.visibility = View.VISIBLE
             }*/
            adView.setNativeAd(amNative)

        }else{
            amLayout.visibility = View.GONE
            nativeAdsCallBack?.onAdFailed()
        }

    }

    interface NativeAdsCallBack{
        fun onAdLoaded()
        fun onAdFailed()
    }

}