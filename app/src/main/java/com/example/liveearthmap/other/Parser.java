package com.example.liveearthmap.other;

import org.json.JSONException;
import org.json.JSONObject;

public class Parser {

    public static String getString(JSONObject jsonObject, String key) {

        try {
            if (jsonObject.has(key) && !jsonObject.isNull(key)) {
                return jsonObject.getString(key);
            }
        } catch (JSONException ignored) {
        }
        return "";
    }

    public static double getDouble(JSONObject jsonObject, String key) {

        try {
            if (jsonObject.has(key) && !jsonObject.isNull(key)) {
                return jsonObject.getDouble(key);
            }
        } catch (JSONException ignored) {
        }
        return 0.0;
    }

    public static Long getLong(JSONObject jsonObject, String key) {

        try {
            if (jsonObject.has(key) && !jsonObject.isNull(key)) {
                return jsonObject.getLong(key);
            }
        } catch (JSONException ignored) {
        }
        return 0L;
    }

    public static int getInt(JSONObject jsonObject, String key) {

        try {
            if (jsonObject.has(key) && !jsonObject.isNull(key)) {
                return jsonObject.getInt(key);
            }
        } catch (JSONException ignored) {
        }
        return -1;
    }

    public static boolean getBoolean(JSONObject jsonObject, String key) {

        try {
            if (jsonObject.has(key) && !jsonObject.isNull(key)) {
                return jsonObject.getBoolean(key);
            }
        } catch (JSONException ignored) {
        }
        return false;
    }

}
