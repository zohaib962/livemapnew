package com.example.liveearthmap.other;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class MyLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final long UPDATE_INTERVAL = 6000, FASTEST_INTERVAL = 300;
    private GoogleApiClient googleApiClient;
    Activity activity;

    public MyLocation(Context context, OnLocationUpdate onLocationUpdate,Activity activity) {
        this.activity=activity;
        this.onLocationUpdate = onLocationUpdate;

        googleApiClient = new GoogleApiClient.Builder(context).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();

    }

    public void start() {
        isStop = false;
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    boolean isStop;

    public void stop() {
        isStop = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (googleApiClient != null && googleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener);
                    googleApiClient.disconnect();
                }

            }
        }, 3000);
    }


    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (isStop) {
                return;
            }
            if (onLocationUpdate != null && location != null)
                onLocationUpdate.locationChange(location);
        }
    };

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        if (isStop) {
            return;
        }
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        // **************************
        builder.setAlwaysShow(true); // this is the key ingredient
        // **************************

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result
                        .getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location
                        // requests here.
                        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(activity, Constants.INSTANCE.getREQUEST_CHECK_SETTINGS());
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        Toast.makeText(
                                activity,
                                "Your device does not support gps",
                                Toast.LENGTH_SHORT
                        ).show();
                        break;
                }
            }
        });




    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (isStop) {
            return;
        }
        @SuppressLint("MissingPermission") Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (onLocationUpdate != null && lastLocation != null)
            onLocationUpdate.locationChange(lastLocation);
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

        if (onLocationUpdate != null)
            onLocationUpdate.connectionSuspended(i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (onLocationUpdate != null)
            onLocationUpdate.connectionFailed(connectionResult);
    }


    public interface OnLocationUpdate {
        void locationChange(Location location);

        void connectionSuspended(int i);

        void connectionFailed(ConnectionResult connectionResult);
    }

    private OnLocationUpdate onLocationUpdate;


}
