package com.example.liveearthmap.other

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.liveearthmap.ExitDialog

object Constants {
    val REQUEST_CHECK_SETTINGS = 0x1
    var AM="am"
    var AM_HCTR="am_hctr"
    var AM_LCTR="am_lctr"
    var AL="al"
    var AM_AL="am_al"
    fun showExitDialog(activity: Activity){
        val dialog = ExitDialog(activity)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        val window = dialog.window
        window?.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
    }

}