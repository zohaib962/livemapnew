package com.example.liveearthmap.other

/**
 * Created by Zohaib on 1/10/2021.
 */
object RemoteKeys {

    var lem_applovin_banner="al"
    var lem_applovin_banner_map="al"


    var lem_splash_inter_new="no" //am,al,am_al
    var lem_speed_inter_new="al"
    var lem_weather_inter_new="al"
    var lem_satellite_inter_new="al"
    var lem_wonder_inter_new="al"
    var lem_shareaddress_inter_new="al"
    var lem_altitude_inter_new="al"
    var lem_compass_inter_new="al"
    var lem_saveaddress_inter_new="al"

    var lem_exit_inter="al"
    var lem_premium_exit_inter_new="al" //am,al,am_al
    var lem_altitude_exit_inter_new="al" //am,al,am_al
    var lem_compass_exit_inter_new="al" //am,al,am_al
    var lem_satellite_exit_inter_new="no" //am,al,am_al
    var lem_saveaddress_exit_inter_new="al" //am,al,am_al
    var lem_wonder_exit_inter_new="al" //am,al,am_al
    var lem_shareaddress_exit_inter_new="al" //am,al,am_al
    var lem_speed_exit_inter_new="al" //am,al,am_al
    var lem_weather_exit_inter_new="al" //am,al,am_al

    var lem_splash_native_new="al" //am,am_hctr,am_lctr,al,am_al
    var lem_dash_native_new="al" //am,al,am_al
    var lem_permission_native_new="al"
    var lem_saveaddress_native_new="al"
    var lem_exit_native_new="al"
    var lem_wonder_native_new="al"
    var lem_weather_native_new="al"
    var lem_wonder_inner_native_new="al"

    var lem_emoji_inter_new="al"


    //    native size
    //medium,small,full
    var splash_native_size = "medium"

}