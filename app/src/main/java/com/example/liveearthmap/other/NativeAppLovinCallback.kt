package com.example.liveearthmap.other

interface NativeAppLovinCallback {
   fun onLoaded()
   fun onFailed()
}