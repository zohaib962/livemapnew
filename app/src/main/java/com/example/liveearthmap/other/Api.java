package com.example.liveearthmap.other;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;


public class Api {

    public static void postCall(Context context, final String url, final String body, final VolleyCallBack volleyCallBack) {


        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallBack.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyCallBack.onError(error);

            }
        }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return super.getHeaders();
            }*/

            @Override
            public byte[] getBody() {
                return body.getBytes();
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    public static void getCall(Context context, final String url, final String body, final VolleyCallBack volleyCallBack) {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url,  new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallBack.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyCallBack.onError(error);

            }
        }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if (body.equals(""))
                    return super.getBody();
                else
                    return body.getBytes();
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    public static void patchCall(Context context, final String url, final String body, final VolleyCallBack volleyCallBack) {

        StringRequest strReq = new StringRequest(Request.Method.PATCH,
                url,  new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallBack.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyCallBack.onError(error);

            }
        }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return super.getHeaders();

            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if (body.equals(""))
                    return super.getBody();
                else
                    return body.getBytes();
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    public static void deleteCall(Context context, final String url, final String body, final VolleyCallBack volleyCallBack) {

        StringRequest strReq = new StringRequest(Request.Method.DELETE,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallBack.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyCallBack.onError(error);

            }
        }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return super.getHeaders();
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if (body.equals(""))
                    return super.getBody();
                else
                    return body.getBytes();
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);
    }

    public interface VolleyCallBack {
        void onResponse(String response);

        void onError(VolleyError error);
    }

}
