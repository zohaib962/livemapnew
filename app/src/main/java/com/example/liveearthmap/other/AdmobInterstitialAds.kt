package com.example.liveearthmap.other

import android.app.Activity
import android.content.Context
import android.util.Log
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback

object AdmobInterstitialAds {


    private var admobInter: InterstitialAd? = null
    var adsCheckFirstTime = true

    fun loadAdmobInterstitial(context: Activity, adId:String) {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            context,
            adId,
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.e("interstitial_admob", adError.message + adError.code)
                    admobInter = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.e("interstitial_admob", "Ad was loaded.")
                    admobInter = interstitialAd
                }
            })
    }

    fun showAdmobInterstitial(
        activity: Activity,
        adValue: String,
        interstitialAdsCallBack: InterstitialAdsCallBack
    ) {
        if (admobInter != null && adValue.contains(Constants.AM)) {
            admobInter?.show(activity)

            admobInter?.fullScreenContentCallback = object : FullScreenContentCallback() {
                override fun onAdDismissedFullScreenContent() {
                    Log.e("admob_interstitial_show", "Ad was dismissed.")
                    interstitialAdsCallBack.onResult(false)
                }

                override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                    Log.e(
                        "admob_interstitial_show",
                        "Ad failed to show." + adError?.message + adError?.code
                    )
                    interstitialAdsCallBack.onResult(true)

                }

                override fun onAdShowedFullScreenContent() {
                    Log.e("admob_interstitial_show", "Ad showed fullscreen content.")
                    admobInter = null
                    loadAdmobInterstitial(activity,SharePrefData.getInstance().admobInterInner.toString().trim())
                }

            }

        } else {
            Log.e("admob_interstitial_show", "The interstitial ad wasn't ready yet.")
            if(adValue == Constants.AM_AL){
                interstitialAdsCallBack.onResult(true)
            }else {
                interstitialAdsCallBack.onResult(false)
            }
            if (!SharePrefData.getInstance().adS_PREFS && adsCheckFirstTime) {
                adsCheckFirstTime=false
                loadAdmobInterstitial(activity,SharePrefData.getInstance().admobInterInner.toString().trim())
            }
        }
    }

    interface InterstitialAdsCallBack {
        fun onResult(showAppLovin:Boolean)
    }

}