package com.example.liveearthmap.other;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.liveearthmap.R;


public class SaveValue {


    private final String FIRST_TIME = "firstTime";
    private final String DARK_THEME = "darkTheme";

    private SharedPreferences mPref;
    private SharedPreferences.Editor editor;

    public SaveValue(Context mContext) {
        mPref = mContext.getSharedPreferences(mContext.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = mPref.edit();
    }


    public void setFirstTime(boolean b) {
        editor.putBoolean(FIRST_TIME, b).apply();
    }

    public boolean getFirstTime() {
        return mPref.getBoolean(FIRST_TIME, true);
    }

    public void setDarkTheme(boolean b) {
        editor.putBoolean(DARK_THEME, b).apply();
    }

    public boolean isDarkTheme() {
        return mPref.getBoolean(DARK_THEME, false);
    }


}
