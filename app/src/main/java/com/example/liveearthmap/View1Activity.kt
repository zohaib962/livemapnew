package com.example.liveearthmap

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_splash_1.*

class View1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {


        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController!!.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_1)
        start.setOnClickListener {
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }

        /* YoYo.with(Techniques.SlideInRight)
             .duration(100)
             .repeat(1)
             .playOn(text3)

         YoYo.with(Techniques.SlideInRight)
             .duration(1500)
             .repeat(1)
             .playOn(text5)

         YoYo.with(Techniques.SlideInRight)
             .duration(2000)
             .repeat(1)
             .playOn(text4)
 */

        text3.visibility = View.VISIBLE
        text3.startAnimation(
            AnimationUtils.loadAnimation(applicationContext, R.anim.splash_anim)
        )
        text5.visibility = View.VISIBLE

        text5.startAnimation(
            AnimationUtils.loadAnimation(applicationContext, R.anim.splash_anim_1)
        )
        text4.visibility = View.VISIBLE

        text4.startAnimation(
            AnimationUtils.loadAnimation(
                applicationContext, R.anim.splash_anim_2
            )
        )
    }
}