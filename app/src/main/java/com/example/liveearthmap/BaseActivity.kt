package com.example.liveearthmap

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.liveearthmap.other.*
import kotlinx.android.synthetic.main.custom_toolbar.*

abstract class BaseActivity : AppCompatActivity() {


    protected lateinit var saveValue: SaveValue
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        saveValue = SaveValue(applicationContext)




    }

    override fun onStart() {
        super.onStart()

        if (adRemove!=null){

            adRemove.setOnClickListener {

                InterstitialAdsAppLovin.showInterstitial(
                    this,
                    RemoteKeys.lem_emoji_inter_new,
                    object : InterstitialAppLovinCallback {
                        override fun onResult() {
                        }

                    })

            }

        }
    }

   


}