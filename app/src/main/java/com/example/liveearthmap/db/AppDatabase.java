package com.example.liveearthmap.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.liveearthmap.model.AddressDetail;

@Database(entities = { AddressDetail.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TaskDao taskDao();
}
