package com.example.liveearthmap.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.liveearthmap.model.AddressDetail;

import java.util.List;

@Dao
public interface TaskDao {


    @Insert
    void insertAddress(AddressDetail addressDetail);


    @Query("SELECT * FROM AddressDetail")
    List<AddressDetail> getAllAddress();

    @Query("DELETE FROM AddressDetail WHERE id=:id")
    void deleteAddress(int id);




}
