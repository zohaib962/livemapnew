package com.example.liveearthmap.db;

import android.content.Context;

public class DB {
    public static TaskDao get(Context context){

        return DatabaseClient.getInstance(context).getAppDatabase()
                .taskDao();
    }
}
