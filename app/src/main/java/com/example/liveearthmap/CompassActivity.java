package com.example.liveearthmap;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxAdView;
import com.example.liveearthmap.compass.sensor.SensorListener;
import com.example.liveearthmap.compass.sensor.SensorUtil;
import com.example.liveearthmap.compass.view.CompassView2;
import com.example.liveearthmap.other.AdmobInterstitialAds;
import com.example.liveearthmap.other.Constants;
import com.example.liveearthmap.other.InterstitialAdsAppLovin;
import com.example.liveearthmap.other.InterstitialAppLovinCallback;
import com.example.liveearthmap.other.RemoteKeys;
import com.example.liveearthmap.other.SharePrefData;


public class CompassActivity extends BaseActivity implements SensorListener.OnValueChangedListener, MaxAdViewAdListener {


    private CompassView2 mCompassView;
    private SensorListener mSensorListener;

    private TextView titleText;
    private ImageView toolbarImage;

    private MaxAdView adView;
    FrameLayout rootView;

    private void loadAppLovinBannerAd(){
        adView = new MaxAdView( SharePrefData.getInstance().getApplovinBannerr(), this );
        adView.setListener( this );
        // Stretch to the width of the screen for banners to be fully functional
        int width = ViewGroup.LayoutParams.MATCH_PARENT;

        // Banner height on phones and tablets is 50 and 90, respectively
        int heightPx = getResources().getDimensionPixelSize( R.dimen.banner_height );

        adView.setLayoutParams( new FrameLayout.LayoutParams( width, heightPx ) );

        // Set background or background color for banners to be fully functional
        adView.setBackgroundColor(ContextCompat.getColor(this,R.color.white));


        rootView.addView( adView );

        // Load the ad
        adView.loadAd();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_compass);
        rootView = findViewById(R.id.bannerad);
        if(RemoteKeys.INSTANCE.getLem_applovin_banner().equals(Constants.INSTANCE.getAL()) && !SharePrefData.getInstance().getApplovinBannerr().isEmpty())
        {loadAppLovinBannerAd();}
        titleText = findViewById(R.id.titleText);
        toolbarImage = findViewById(R.id.toolbarImage);
        titleText.setText("Compass");
        titleText.setTextColor(Color.WHITE);
        toolbarImage.setColorFilter(Color.WHITE);
        toolbarImage.setImageResource(R.drawable.ic_back);
        toolbarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (hasSensor()) {
            createView();
        } else {
            showDialogUnsupported();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mSensorListener != null) {
            mSensorListener.start();
        }
    }

    @Override
    public void onStop() {
        if (mSensorListener != null) {
            mSensorListener.stop();
        }
        super.onStop();

    }

    @Override
    public void onRotationChanged(float azimuth, float roll, float pitch) {
        mCompassView.getSensorValue().setRotation(azimuth, roll, pitch);
    }

    @Override
    public void onMagneticFieldChanged(float value) {
        mCompassView.getSensorValue().setMagneticField(value);
    }


    private void createView() {

        mCompassView = (CompassView2) findViewById(R.id.compass_view);

        mSensorListener = new SensorListener(this);
        mSensorListener.setOnValueChangedListener(this);
    }

    private boolean hasSensor() {
        return SensorUtil.hasAccelerometer(this) && SensorUtil.hasMagnetometer(this);
    }

    private void showDialogUnsupported() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your device unsupported Accelerometer sensor and Magnetometer");
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).create().show();
    }

    @Override
    public void onBackPressed() {

        if (RemoteKeys.INSTANCE.getLem_compass_exit_inter_new().contains(Constants.INSTANCE.getAM())) {
            AdmobInterstitialAds.INSTANCE.showAdmobInterstitial(
                    this,
                    RemoteKeys.INSTANCE.getLem_compass_exit_inter_new(),
                    new AdmobInterstitialAds.InterstitialAdsCallBack() {
                        @Override
                        public void onResult(boolean showAppLovin) {
                            if (RemoteKeys.INSTANCE.getLem_compass_exit_inter_new().equals(Constants.INSTANCE.getAM_AL()) && showAppLovin) {
                                InterstitialAdsAppLovin.INSTANCE.showInterstitial(
                                        CompassActivity.this,
                                        RemoteKeys.INSTANCE.getLem_compass_exit_inter_new(),
                                        new InterstitialAppLovinCallback() {
                                            @Override
                                            public void onResult() {
                                                finish();
                                            }
                                        });
                            } else {
                                finish();
                            }
                        }
                    });
        } else if (RemoteKeys.INSTANCE.getLem_compass_exit_inter_new().equals(Constants.INSTANCE.getAL())) {
            InterstitialAdsAppLovin.INSTANCE.showInterstitial(
                    CompassActivity.this,
                    RemoteKeys.INSTANCE.getLem_compass_exit_inter_new(),
                    new InterstitialAppLovinCallback() {
                        @Override
                        public void onResult() {
                            finish();
                        }
                    });
        } else{
            finish();
        }
    }

    @Override
    public void onAdExpanded(MaxAd ad) {

    }

    @Override
    public void onAdCollapsed(MaxAd ad) {

    }

    @Override
    public void onAdLoaded(MaxAd ad) {

    }

    @Override
    public void onAdDisplayed(MaxAd ad) {

    }

    @Override
    public void onAdHidden(MaxAd ad) {

    }

    @Override
    public void onAdClicked(MaxAd ad) {

    }

    @Override
    public void onAdLoadFailed(String adUnitId, MaxError error) {
        rootView.setVisibility(View.GONE);
    }

    @Override
    public void onAdDisplayFailed(MaxAd ad, MaxError error) {
        rootView.setVisibility(View.GONE);
    }
}
