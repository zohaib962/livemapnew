package com.example.liveearthmap;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.liveearthmap.billing.BillingCallBack;
import com.example.liveearthmap.billing.InAppBillingUtils;
import com.example.liveearthmap.other.AdmobInterstitialAds;
import com.example.liveearthmap.other.Constants;
import com.example.liveearthmap.other.InterstitialAdsAppLovin;
import com.example.liveearthmap.other.InterstitialAppLovinCallback;
import com.example.liveearthmap.other.RemoteKeys;
import com.example.liveearthmap.other.SaveValue;
import com.example.liveearthmap.other.SharePrefData;

public class AdRemoveActivity extends AppCompatActivity{

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_remove2);

        InAppBillingUtils.Companion.setData(this);

        linearLayout=findViewById(R.id.upgradeView);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InAppBillingUtils.Companion.purchase(AdRemoveActivity.this, new BillingCallBack() {
                    @Override
                    public void onBillingPurchases() {
                        refreshApp();
                    }

                    @Override
                    public void onBillingError() {
                        Toast.makeText(AdRemoveActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });



//        View root = findViewById(R.id.root);
//        View root2 = findViewById(R.id.root2);
//
//        SaveValue saveValue = new SaveValue(this);
//        if (saveValue.isDarkTheme()) {
//            root.setBackgroundColor(Color.parseColor("#000230"));
//            root2.setBackgroundColor(Color.parseColor("#000230"));
//        } else {
//            root.setBackgroundColor(Color.WHITE);
//            root2.setBackgroundColor(Color.WHITE);
//        }

        findViewById(R.id.cross).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishActivity();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }


    public void finishActivity() {

        if (RemoteKeys.INSTANCE.getLem_premium_exit_inter_new().contains(Constants.INSTANCE.getAM())) {
            AdmobInterstitialAds.INSTANCE.showAdmobInterstitial(
                    this,
                    RemoteKeys.INSTANCE.getLem_premium_exit_inter_new(),
                    new AdmobInterstitialAds.InterstitialAdsCallBack() {
                        @Override
                        public void onResult(boolean showAppLovin) {
                            if (RemoteKeys.INSTANCE.getLem_premium_exit_inter_new().equals(Constants.INSTANCE.getAM_AL()) && showAppLovin) {
                                InterstitialAdsAppLovin.INSTANCE.showInterstitial(
                                        AdRemoveActivity.this,
                                        RemoteKeys.INSTANCE.getLem_premium_exit_inter_new(),
                                        new InterstitialAppLovinCallback() {
                                            @Override
                                            public void onResult() {
                                                finish();
                                            }
                                        });
                            } else {
                                finish();
                            }
                        }
                    });
        } else if (RemoteKeys.INSTANCE.getLem_premium_exit_inter_new().equals(Constants.INSTANCE.getAL())) {
            InterstitialAdsAppLovin.INSTANCE.showInterstitial(
                    AdRemoveActivity.this,
                    RemoteKeys.INSTANCE.getLem_premium_exit_inter_new(),
                    new InterstitialAppLovinCallback() {
                        @Override
                        public void onResult() {
                            finish();
                        }
                    });
        } else{
            finish();
        }


    }

    public void refreshApp(){
        SharePrefData.getInstance().setADS_PREFS(true);
        Intent intent = new Intent(this, StartActivity.class);
        this.startActivity(intent);
        this.finishAffinity();
    }
}