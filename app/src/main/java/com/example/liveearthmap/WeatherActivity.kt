package com.example.liveearthmap

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.VolleyError
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.example.liveearthmap.adapter.WeatherAdapter
import com.example.liveearthmap.model.WeatherData
import com.example.liveearthmap.model.WeatherHourly
import com.example.liveearthmap.model.WeatherHourly.AllList
import com.example.liveearthmap.other.*
import com.example.liveearthmap.other.Api.VolleyCallBack
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.common.ConnectionResult
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_weather.*
import kotlinx.android.synthetic.main.activity_weather.admobNativeView
import kotlinx.android.synthetic.main.activity_weather.bannerad
import kotlinx.android.synthetic.main.activity_weather.mainAdLayout
import kotlinx.android.synthetic.main.activity_weather.noAdView
import kotlinx.android.synthetic.main.activity_weather.root
import kotlinx.android.synthetic.main.custom_toolbar.*
import org.json.JSONException
import org.json.JSONObject


class WeatherActivity : BaseActivity(), VolleyCallBack, MyLocation.OnLocationUpdate {

    lateinit var myLocation: MyLocation
//    private var list: ArrayList<WeatherData> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)


        if (!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
        }

        titleText.setTextColor(Color.WHITE)
        toolbarImage.setColorFilter(Color.WHITE)
        toolbarImage.setImageResource(R.drawable.ic_back)
        toolbarImage.setOnClickListener {

          onBackPressed()

        }
        if (saveValue.isDarkTheme) {

            root.setBackgroundColor((Color.parseColor("#000230")))
        } else {
            root.setBackgroundResource(R.drawable.main_weather_bg)
        }

        weatherRecyclerView.layoutManager = LinearLayoutManager(
            applicationContext,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        myLocation = MyLocation(applicationContext, this,this);
        myLocation.start()


        NativeAdsApplovin.showNativeAd(this,this,RemoteKeys.lem_weather_native_new,admobNativeView,object :
            NativeAppLovinCallback {
            override fun onLoaded() {
                mainAdLayout.visibility=View.VISIBLE
            }

            override fun onFailed() {
                mainAdLayout.visibility=View.GONE
            }

        })


        noAdView.setOnClickListener {
            startActivity(Intent(this,AdRemoveActivity::class.java))
        }



    }

    private fun setCurrentData(weatherData: WeatherData) {

        if (weatherData.root.name == "") {
            cityName.text = "---"
        } else {
            cityName.text = weatherData.root.name
        }

        if (weatherData.root.weather.size > 0) {

            if (weatherData.root.weather[0].main == "")
                weatherDec.text = "---"
            else
                weatherDec.text = weatherData.root.weather[0].main

            if (weatherData.root.weather[0].icon != "") {
                weatherImage.setImageResource(Utils.getIcon(weatherData.root.weather[0].icon))
//                var s =
//                    "http://openweathermap.org/img/w/" + weatherData.root.weather[0].icon + ".png"
//                Picasso.get().load(s).into(weatherImage)
            }


        }


        if (weatherData.root.main.temp != 0.0) {
                temp.text = (weatherData.root.main.temp - 273.15).toInt().toString() + "°"
        } else {
            temp.text = "---"
        }

        if (weatherData.root.wind.speed != 0.0) {
            wind.text = (weatherData.root.wind.speed * 3.6).toInt().toString() + "Km/h"
        } else {
            wind.text = "---";
        }

        if (weatherData.root.main.humidity == 0) {
            humidity.text = "---";
        } else {
            humidity.text = weatherData.root.main.humidity.toString() + "%"

        }

        if (weatherData.root.main.temp_max == 0.0) {

            maximumTemp.text = "---"
        } else {
            maximumTemp.text = (weatherData.root.main.temp_max - 273.15).toInt().toString() + "°"
        }


    }

    override fun onResponse(response: String?) {
        try {
            val weatherHourly = WeatherHourly()
            val jsonObject = JSONObject(response)
            if (jsonObject.has("cod")) {
                weatherHourly.root.cod = Parser.getString(jsonObject, "cod")
            }
            if (jsonObject.has("message")) {
                weatherHourly.root.message = Parser.getDouble(jsonObject, "message")
            }
            if (jsonObject.has("cnt")) {
                weatherHourly.root.cnt = Parser.getInt(jsonObject, "cnt")
            }
            if (jsonObject.has("list")) {
                val dataList = jsonObject.getJSONArray("list")
                for (i in 0 until dataList.length()) {
                    val list = AllList()
                    val data = dataList.getJSONObject(i)
                    list.dt = Parser.getLong(data, "dt")
                    if (data.has("main")) {
                        val main = data.getJSONObject("main")
                        list.main.temp = Parser.getDouble(main, "temp")
                        list.main.temp_min = Parser.getDouble(main, "temp_min")
                        list.main.temp_max = Parser.getDouble(main, "temp_max")
                        list.main.pressure = Parser.getDouble(main, "pressure")
                        list.main.sea_level = Parser.getDouble(main, "sea_level")
                        list.main.grnd_level = Parser.getDouble(main, "grnd_level")
                        list.main.humidity = Parser.getInt(main, "humidity")
                        list.main.temp_kf = Parser.getDouble(main, "temp_kf")
                    }
                    if (data.has("weather")) {
                        val weather = data.getJSONArray("weather")
                        for (j in 0 until weather.length()) {
                            val weather1 = WeatherHourly.Weather()
                            val singleWeather = weather.getJSONObject(j)
                            weather1.description =
                                Parser.getString(singleWeather, "description")
                            weather1.icon = Parser.getString(singleWeather, "icon")
                            weather1.main = Parser.getString(singleWeather, "main")
                            weather1.id = Parser.getInt(singleWeather, "id")
                            list.weather.add(weather1)
                        }
                    }
                    if (data.has("clouds")) {
                        val clouds = data.getJSONObject("clouds")
                        list.clouds.all = Parser.getInt(clouds, "all")
                    }
                    if (data.has("wind")) {
                        val wind = data.getJSONObject("wind")
                        list.wind.speed = Parser.getDouble(wind, "speed")
                        list.wind.deg = Parser.getDouble(wind, "deg")
                    }
                    if (data.has("rain")) {
                        val rain = data.getJSONObject("rain")
                        list.rain.oneH = Parser.getDouble(rain, "1h")
                    }
                    if (data.has("sys")) {
                        val sys = data.getJSONObject("sys")
                        list.sys.pod = Parser.getString(sys, "pod")
                    }

//                            list.getRain()
                    list.dt_txt = Parser.getString(data, "dt_txt")
                    weatherHourly.root.list.add(list)
                }
            }
            if (jsonObject.has("city")) {
                val city = jsonObject.getJSONObject("city")
                weatherHourly.root.city.id = Parser.getInt(city, "id")
                weatherHourly.root.city.name = Parser.getString(city, "name")
                weatherHourly.root.city.country = Parser.getString(city, "country")
                if (city.has("coord")) {
                    val city1 = city.getJSONObject("coord")
                    weatherHourly.root.city.coord.lat =
                        Parser.getDouble(city1, "lat")
                    weatherHourly.root.city.coord.lon =
                        Parser.getDouble(city1, "lon")
                }
            }


            weatherRecyclerView.adapter = WeatherAdapter(weatherHourly.root.list)

        } catch (e: JSONException) {
            Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }


    }

    override fun onError(error: VolleyError?) {
        error!!.printStackTrace()
        Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_LONG).show()
    }

    var go: Boolean = false
    override fun locationChange(location: Location?) {
        if (go)
            return

        go = true
        Api.getCall(
            applicationContext,
            URL.BASE_URL + "weather?lat=" +
                    location!!.latitude +
                    "&lon=" +
                    location!!.longitude +
                    "&appid=" + URL.API_KEY,
            "",
            object : VolleyCallBack {
                override fun onResponse(response: String) {
                    try {
                        val weatherData = WeatherData()
                        val jsonObject = JSONObject(response)
                        if (jsonObject.has("coord")) {
                            val coord = jsonObject.getJSONObject("coord")
                            weatherData.getRoot().getCoordinate()
                                .setLat(Parser.getDouble(coord, "lon"))
                            weatherData.getRoot().getCoordinate()
                                .setLon(Parser.getDouble(coord, "lat"))
                        }
                        if (jsonObject.has("weather")) {
                            val weathers = jsonObject.getJSONArray("weather")
                            for (i in 0 until weathers.length()) {
                                val weather = weathers.getJSONObject(i)
                                val singleWeatherData = WeatherData.Weather()
                                singleWeatherData.description =
                                    Parser.getString(weather, "description")
                                singleWeatherData.icon =
                                    Parser.getString(weather, "icon")
                                singleWeatherData.main =
                                    Parser.getString(weather, "main")
                                singleWeatherData.id = Parser.getInt(weather, "id")
                                weatherData.getRoot().getWeather().add(singleWeatherData)
                            }
                        }
                        if (jsonObject.has("base")) {
                            weatherData.getRoot().setBase(Parser.getString(jsonObject, "base"))
                        }
                        if (jsonObject.has("main")) {
                            val main = jsonObject.getJSONObject("main")
                            weatherData.getRoot().getMain()
                                .setGrnd_level(Parser.getDouble(main, "grnd_level"))
                            weatherData.getRoot().getMain()
                                .setSea_level(Parser.getDouble(main, "sea_level"))
                            weatherData.getRoot().getMain()
                                .setTemp_max(Parser.getDouble(main, "temp_max"))
                            weatherData.getRoot().getMain()
                                .setTemp_min(Parser.getDouble(main, "temp_min"))
                            weatherData.getRoot().getMain()
                                .setHumidity(Parser.getInt(main, "humidity"))
                            weatherData.getRoot().getMain()
                                .setPressure(Parser.getDouble(main, "pressure"))
                            weatherData.getRoot().getMain().setTemp(Parser.getDouble(main, "temp"))
                        }
                        if (jsonObject.has("wind")) {
                            val wind = jsonObject.getJSONObject("wind")
                            weatherData.getRoot().getWind()
                                .setSpeed(Parser.getInt(wind, "speed").toDouble())
                            weatherData.getRoot().getWind().setDeg(Parser.getInt(wind, "deg"))
                        }
                        if (jsonObject.has("clouds")) {
                            val clouds = jsonObject.getJSONObject("clouds")
                            weatherData.getRoot().getClouds().setAll(Parser.getInt(clouds, "all"))
                        }

                        weatherData.getRoot().setDt(Parser.getLong(jsonObject, "dt"))

                        if (jsonObject.has("sys")) {
                            val sys = jsonObject.getJSONObject("sys")
                            weatherData.getRoot().getSys()
                                .setCountry(Parser.getString(sys, "country"))
                            weatherData.getRoot().getSys()
                                .setMessage(Parser.getDouble(sys, "message"))
                            weatherData.getRoot().getSys().setSunrise(Parser.getInt(sys, "sunrise"))
                            weatherData.getRoot().getSys().setSunset(Parser.getInt(sys, "sunset"))
                        }
                        if (jsonObject.has("id")) {
                            weatherData.getRoot().setId(Parser.getInt(jsonObject, "id"))
                        }
                        if (jsonObject.has("name")) {
                            weatherData.getRoot().setName(Parser.getString(jsonObject, "name"))
                        }
                        if (jsonObject.has("cod")) {
                            weatherData.getRoot().setCod(Parser.getInt(jsonObject, "cod"))
                        }


                        setCurrentData(weatherData);


                    } catch (e: JSONException) {
                    }
                }

                override fun onError(error: VolleyError) {
                    error.printStackTrace()
                }
            })
        Api.getCall(
            applicationContext,
            URL.BASE_URL + "forecast?lat=" + location!!.latitude +
                    "&lon=" + location!!.longitude +
                    "&appid=" + URL.API_KEY,
            "",
            this
        )


        var handler = Handler()
        handler.postDelayed(Runnable { myLocation.stop() }, 3000)
    }

    override fun connectionSuspended(i: Int) {

    }

    override fun connectionFailed(connectionResult: ConnectionResult?) {

    }

    override fun onBackPressed() {

        if (RemoteKeys.lem_weather_exit_inter_new.contains(Constants.AM, true)) {
            AdmobInterstitialAds.showAdmobInterstitial(
                this,
                RemoteKeys.lem_weather_exit_inter_new,
                object : AdmobInterstitialAds.InterstitialAdsCallBack {
                    override fun onResult(showAppLovin: Boolean) {
                        if (RemoteKeys.lem_weather_exit_inter_new == Constants.AM_AL && showAppLovin) {
                            InterstitialAdsAppLovin.showInterstitial(
                                this@WeatherActivity,
                                RemoteKeys.lem_weather_exit_inter_new,
                                object : InterstitialAppLovinCallback {
                                    override fun onResult() {
                                        finish()
                                    }

                                })
                        } else {
                            finish()
                        }
                    }
                })
        } else if (RemoteKeys.lem_weather_exit_inter_new == Constants.AL) {
            InterstitialAdsAppLovin.showInterstitial(
                this,
                RemoteKeys.lem_weather_exit_inter_new,
                object : InterstitialAppLovinCallback {
                    override fun onResult() {
                        finish()
                    }

                })
        }else{
            finish()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.REQUEST_CHECK_SETTINGS) {

            myLocation = MyLocation(applicationContext, this,this);
            myLocation.start()

        }


    }


    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

}