package com.example.liveearthmap.network;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AlitudeResponse{

	@SerializedName("data")
	private List<Integer> data;

	@SerializedName("status")
	private String status;

	public List<Integer> getData(){
		return data;
	}

	public String getStatus(){
		return status;
	}
}