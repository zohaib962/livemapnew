package com.example.liveearthmap.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Zohaib on 1/6/2021.
 */
public interface ApiInterfave {

    @GET("elevation/v1/ele")
    public Call<AlitudeResponse> getAltitude(@Query("points") String latlng);

}
