package com.example.liveearthmap

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.bumptech.glide.Glide
import com.example.liveearthmap.model.WonderData
import com.example.liveearthmap.other.*
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.activity_wonder_detail.*
import kotlinx.android.synthetic.main.activity_wonder_detail.admobNativeView
import kotlinx.android.synthetic.main.activity_wonder_detail.bannerad
import kotlinx.android.synthetic.main.activity_wonder_detail.mainAdLayout
import kotlinx.android.synthetic.main.activity_wonder_detail.noAdView
import kotlinx.android.synthetic.main.activity_wonder_detail.root
import kotlinx.android.synthetic.main.custom_toolbar.*
import java.lang.Exception


class WonderDetailActivity : BaseActivity(), ConfirmDialog.ClickInterface {

    var data: WonderData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wonder_detail)

        if (!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
        }

        data = intent.getSerializableExtra("data") as? WonderData


        titleText.text = data!!.name
        toolbarImage.setImageResource(R.drawable.ic_back)
        toolbarImage.setOnClickListener { finish() }

        if (saveValue.isDarkTheme) {
            titleText.setTextColor(Color.WHITE)
            toolbarImage.setColorFilter(Color.WHITE)
            root.setBackgroundColor((Color.parseColor("#000230")))
        } else {

            root.setBackgroundColor(Color.WHITE)
        }

        moreInfo.setOnClickListener {


            val dialog = ConfirmDialog(this, this)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
            val window = dialog.window
            window?.setLayout(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
            )

        }

        satelliteView.setOnClickListener {
            try {

                val gmmIntentUri =
                    Uri.parse("google.streetview:cbll=" + data!!.latitude + "," + data!!.longitude + "")
                val mapIntent =
                    Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            }catch (e:ActivityNotFoundException){
                Toast.makeText(this,"No application found to open this feature",Toast.LENGTH_SHORT).show()
            }catch (e:Exception){
                Toast.makeText(this,"No application found to open this feature",Toast.LENGTH_SHORT).show()
            }
        }



        Glide.with(applicationContext).load(data!!.url).into(image)


        play.setOnClickListener {
            play.visibility = View.GONE
            progresss.visibility = View.VISIBLE
            playAndPause.visibility = View.GONE
            videoView.setVideoURI(Uri.parse(data!!.url))
            videoView.setOnPreparedListener {

                videoView.start()
                image.visibility = View.GONE
                progresss.visibility = View.GONE
                playAndPause.visibility = View.VISIBLE
                playAndPause.setImageResource(R.drawable.ic_pause)


            }
        }

        playAndPause.setOnClickListener {


            image.visibility = View.GONE

            if (videoView.isPlaying) {
                playAndPause.setImageResource(R.drawable.ic_play_button)
                videoView.pause()
            } else {
                playAndPause.setImageResource(R.drawable.ic_pause)
                videoView.start()
            }


        }

        NativeAdsApplovin.showNativeAd(this,this,RemoteKeys.lem_wonder_inner_native_new,admobNativeView,object :
            NativeAppLovinCallback {
            override fun onLoaded() {
                mainAdLayout.visibility=View.VISIBLE
            }

            override fun onFailed() {
                mainAdLayout.visibility=View.GONE
            }

        })


        noAdView.setOnClickListener {
            startActivity(Intent(this,AdRemoveActivity::class.java))
        }


    }

    override fun clicked() {
        if (data != null) {
            val url: String = "https://en.wikipedia.org/wiki/" + data!!.name.replace(" ", "_")
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }


    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

}