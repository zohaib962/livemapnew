package com.example.liveearthmap

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.example.liveearthmap.network.AlitudeResponse
import com.example.liveearthmap.network.ApiInterfave
import com.example.liveearthmap.network.RetrofitController
import com.example.liveearthmap.other.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_altitude.*
import kotlinx.android.synthetic.main.activity_altitude.bannerad
import kotlinx.android.synthetic.main.activity_altitude.currentLocation
import kotlinx.android.synthetic.main.activity_altitude.root
import kotlinx.android.synthetic.main.activity_altitude.zoomIn
import kotlinx.android.synthetic.main.activity_altitude.zoomOut
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.search_location.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.lang.NullPointerException

open class AltitudeActivity : LocationActivity(), OnMapReadyCallback {


    override fun updateLocation(location: Location?) {


        var apiInterface = RetrofitController.getClient().create(ApiInterfave::class.java)

        val call: Call<AlitudeResponse> =
            apiInterface.getAltitude(location?.latitude.toString() + "," + location?.longitude.toString());
        call.enqueue(object : Callback<AlitudeResponse?> {
            override fun onFailure(call: Call<AlitudeResponse?>?, t: Throwable?) {
                Toast.makeText(this@AltitudeActivity, "Something went wrong!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<AlitudeResponse?>?,
                response: Response<AlitudeResponse?>?
            ) {
                if (response != null && response.isSuccessful) {
                    try {
                        var altitudeResponse = response.body() as AlitudeResponse

                        if (altitudeResponse.status.equals("success")) {
                            var altitudeList = altitudeResponse.data
                            if (altitudeList != null) {
                                try {
                                    altitude.text = altitudeList.get(0).toString() + " m"
                                } catch (e: NullPointerException) {
                                    e.printStackTrace()
                                    altitude.text = "Not available"
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                    altitude.text = "Not available"
                                }
                            }
                        }
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                        altitude.text = "Not available"
                    } catch (e: Exception) {
                        e.printStackTrace()
                        altitude.text = "Not available"
                    }

                }

            }

        })

//
//        if (location != null)
//            altitude.text = String.format("%.2f", location.altitude)
    }

    override fun updateLocationAltidtude(location: LatLng?) {
        var apiInterface = RetrofitController.getClient().create(ApiInterfave::class.java)

        val call: Call<AlitudeResponse> =
            apiInterface.getAltitude(location?.latitude.toString() + "," + location?.longitude.toString());
        Log.d("locationnn", location?.latitude.toString() + "," + location?.longitude.toString())
        call.enqueue(object : Callback<AlitudeResponse?> {
            override fun onFailure(call: Call<AlitudeResponse?>?, t: Throwable?) {
                Toast.makeText(this@AltitudeActivity, "Something went wrong!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<AlitudeResponse?>?,
                response: Response<AlitudeResponse?>?
            ) {
                try {
                    if (response != null && response.isSuccessful) {
                        var altitudeResponse = response.body() as AlitudeResponse

                        if (altitudeResponse.status.equals("success")) {
                            var altitudeList = altitudeResponse.data
                            altitude.text = altitudeList.get(0).toString() + " m"
                        }


                    }
                } catch (e: NullPointerException) {
                    altitude.text = "Not available"
                } catch (e: Exception) {
                    altitude.text = "Not available"
                }

            }

        })
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_altitude)

        if (!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
        }

        Utils.showMapToast(this)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        titleText.text = "Altitude meter"
        toolbarImage.setImageResource(R.drawable.ic_back)
        toolbarImage.setOnClickListener {
            onBackPressed()
        }

        if (saveValue.isDarkTheme) {
            titleText.setTextColor(Color.WHITE)
            toolbarImage.setColorFilter(Color.WHITE)
            root.setBackgroundColor((Color.parseColor("#000230")))
        } else {

            root.setBackgroundColor(Color.WHITE)
        }

        autoCompleteTextView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                search(p0.toString())

            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        currentLocation.setOnClickListener {
            goToCurrentLocation()
        }

        zoomIn.setOnClickListener {
            if (mMap != null) {
                mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
            }
        }

        zoomOut.setOnClickListener {
            if (mMap != null) {
                mMap!!.animateCamera(CameraUpdateFactory.zoomOut());
            }

        }


        alti.setOnClickListener {

        }

    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if (saveValue.isDarkTheme) {
            googleMap.mapType = GoogleMap.MAP_TYPE_SATELLITE;
        }
        setOnMapClick(googleMap, altitude)

    }


/*

    private val onItemClickListener =
        AdapterView.OnItemClickListener { adapterView, _, _, _ ->

            for (i in 0 until list.size) {
                if (adapterView.getItemAtPosition(i) == list[i].name) {
                    goToLocation(list[i].lat.toDouble(), list[i].lng.toDouble(), list[i].name)
                    break

                }
            }
        }
*/


    override fun onBackPressed() {

        if (RemoteKeys.lem_altitude_exit_inter_new.contains(Constants.AM, true)) {
            AdmobInterstitialAds.showAdmobInterstitial(
                this,
                RemoteKeys.lem_altitude_exit_inter_new,
                object : AdmobInterstitialAds.InterstitialAdsCallBack {
                    override fun onResult(showAppLovin: Boolean) {
                        if (RemoteKeys.lem_altitude_exit_inter_new == Constants.AM_AL && showAppLovin) {
                            InterstitialAdsAppLovin.showInterstitial(
                                this@AltitudeActivity,
                                RemoteKeys.lem_altitude_exit_inter_new,
                                object : InterstitialAppLovinCallback {
                                    override fun onResult() {
                                        finish()
                                    }

                                })
                        } else {
                            finish()
                        }
                    }
                })
        } else if (RemoteKeys.lem_altitude_exit_inter_new == Constants.AL) {
            InterstitialAdsAppLovin.showInterstitial(
                this,
                RemoteKeys.lem_altitude_exit_inter_new,
                object : InterstitialAppLovinCallback {
                    override fun onResult() {
                        finish()
                    }

                })
        } else{
            finish()
        }


    }


    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner_map == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

}