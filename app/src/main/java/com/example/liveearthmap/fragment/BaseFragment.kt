package com.example.liveearthmap.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.liveearthmap.other.SaveValue

abstract class BaseFragment : Fragment(){
    lateinit var saveValue :SaveValue
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        saveValue = SaveValue(context)
    }

}