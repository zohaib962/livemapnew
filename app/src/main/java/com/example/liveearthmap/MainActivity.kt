package com.example.liveearthmap

import android.Manifest
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.Translator.pushnotification.service.VoiceIds
import com.Translator.pushnotification.service.VoiceIds.Companion.subscribeToTopic
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdFormat
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.applovin.sdk.AppLovinSdkUtils
import com.example.liveearthmap.db.DB
import com.example.liveearthmap.other.*
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.admobNativeView
import kotlinx.android.synthetic.main.activity_main.mainAdLayout
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.drawer_layout.*
import kotlinx.android.synthetic.main.rate_dialog.*


class MainActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener {


    companion object {
        var height: Int = 0
        var width: Int = 0
        var resumeCheck = true
    }

    override fun onResume() {
        super.onResume()


        loadnativeAds(admobNativeView!!)


    }

    private fun loadnativeAds(adView: FrameLayout) {

        if (RemoteKeys.lem_dash_native_new.contains(Constants.AM)) {

            NativeAdsDashboard.showAd(
                this,
                RemoteKeys.lem_dash_native_new,
                adView!!,
                object : NativeAdsDashboard.NativeAdsCallBack {

                    override fun onAdLoaded() {
                        mainAdLayout.visibility = View.VISIBLE
                        admobNativeView?.visibility = View.VISIBLE
                    }

                    override fun onAdFailed() {
                        if (RemoteKeys.lem_dash_native_new == Constants.AM_AL) {
                            loadAppLovinNativeAd(adView)
                        } else {
                            mainAdLayout.visibility = View.GONE
                            fulllayout.visibility = View.GONE
                        }
                    }
                }
            )

        } else if (RemoteKeys.lem_dash_native_new == Constants.AL) {
            loadAppLovinNativeAd(adView)
        }

    }

    private fun loadAppLovinNativeAd(adView: FrameLayout) {
        NativeAdsApplovin.showNativeAd(
            this,
            this,
            RemoteKeys.lem_dash_native_new,
            adView,
            object : NativeAppLovinCallback {
                override fun onLoaded() {
                    mainAdLayout.visibility = View.VISIBLE
                }

                override fun onFailed() {
                    mainAdLayout.visibility = View.GONE
                    fulllayout.visibility = View.GONE
                }

            })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        InterstitialAdsAppLovin.loadInterstitialAd(this)
        if(!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
            createMrecAd()
        }

        VoiceIds.Companion.subscribeToTopic()

        val get = DB.get(applicationContext)


        refreshTheme()
        if (!checkPermission()) {
            startActivity(
                Intent(
                    applicationContext,
                    LocationPermissionActivity::class.java
                )
            )
            finish()
            return
        }

        bg.setOnClickListener { }
        yes.setOnClickListener { finish() }
        no.setOnClickListener {
            bg.visibility = View.GONE
        }


        noAdView.setOnClickListener {
            startActivity(Intent(this, AdRemoveActivity::class.java))
        }

        speedometer.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    SpeedometerActivity::class.java
                ), RemoteKeys.lem_speed_inter_new
            )

        }
        weather.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    WeatherActivity::class.java
                ), RemoteKeys.lem_weather_inter_new
            )
        }
        satelliteView.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    SatelliteViewActivity::class.java
                ), RemoteKeys.lem_satellite_inter_new
            )
        }

        worldWonder.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    SevenWonderActivity::class.java
                ), RemoteKeys.lem_wonder_inter_new
            )
        }
        shareAddress.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    ShareAddressActivity::class.java
                ), RemoteKeys.lem_shareaddress_inter_new
            )
        }
        altitude.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    AltitudeActivity::class.java
                ), RemoteKeys.lem_altitude_inter_new
            )
        }

        compass.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    CompassActivity::class.java
                ), RemoteKeys.lem_compass_inter_new
            )
        }

        saveAddress.setOnClickListener {
            goToNext(
                Intent(
                    applicationContext,
                    SavedAddressListActivity::class.java
                ), RemoteKeys.lem_saveaddress_inter_new
            )
        }

        toolbarImage.setOnClickListener {

            if (drawerLayout.visibility == View.VISIBLE) {
                hideDrawer()
            } else {
                showDrawer()
            }

        }
        view.setOnClickListener {
            hideDrawer()
        }

        drawerRoot.setOnClickListener {
        }




        shareApp.setOnClickListener {
            Utils.shareApp(this)
        }
//        moreApp.setOnClickListener {
//            Toast.makeText(applicationContext, "More app link", Toast.LENGTH_SHORT).show()
//        }
        rateUs.setOnClickListener {
//            Utils.likeApp(applicationContext)
            hideDrawer()
            rateDialog()
        }
        privacyPolicy.setOnClickListener {
            hideDrawer()
            startActivity(Intent(this, PrivacyActivity::class.java))
        }
        feedback.setOnClickListener {
            hideDrawer()
            Utils.likeApp(this)
        }

        removeAds.setOnClickListener {
            hideDrawer()
            startActivity(Intent(applicationContext, AdRemoveActivity::class.java))
        }
        quit.setOnClickListener {
            hideDrawer()
            Constants.showExitDialog(this)


        }


        livesatellite.setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=livesatelliteview.liveearthmap.gpsnavigation.routefinder.livestreetview.liveweathermaps.gps.navigation")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        changeTheme.isChecked = saveValue.isDarkTheme
        changeTheme.setOnCheckedChangeListener(this)


        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        height = displayMetrics.heightPixels
        width = displayMetrics.widthPixels


    }

    private fun goToNext(intent: Intent, remoteKey: String) {

        InterstitialAdsAppLovin.showInterstitial(
            this,
            remoteKey,
            object : InterstitialAppLovinCallback {
                override fun onResult() {
                    startActivity(intent)
                }

            })

    }

    private fun refreshTheme() {

        if (saveValue.isDarkTheme) {
            titleText.setTextColor(Color.WHITE)
            quit.setTextColor(Color.WHITE)
            removeAds.setTextColor(Color.WHITE)
            feedback.setTextColor(Color.WHITE)
            privacyPolicy.setTextColor(Color.WHITE)
            rateUs.setTextColor(Color.WHITE)
//            moreApp.setTextColor(Color.WHITE)
            shareApp.setTextColor(Color.WHITE)
            themeTextView.setTextColor(Color.WHITE)

            imageTheme.setColorFilter(Color.WHITE)
            imageShare.setColorFilter(Color.WHITE)
//            imageMore.setColorFilter(Color.WHITE)
            imageRate.setColorFilter(Color.WHITE)
            imagePrivacy.setColorFilter(Color.WHITE)
            imageFeedback.setColorFilter(Color.WHITE)
            imageRemoveAd.setColorFilter(Color.WHITE)


            toolbarImage.setColorFilter(Color.WHITE)
            root.setBackgroundColor((Color.parseColor("#000230")))
            drawerRoot.setBackgroundColor((Color.parseColor("#000230")))


        } else {
            titleText.setTextColor(Color.parseColor("#3D3D3D"))
            rateUs.setTextColor(Color.parseColor("#3D3D3D"))
            themeTextView.setTextColor(Color.parseColor("#3D3D3D"))
            shareApp.setTextColor(Color.parseColor("#3D3D3D"))
//            moreApp.setTextColor(Color.parseColor("#3D3D3D"))
            quit.setTextColor(Color.parseColor("#3D3D3D"))
            removeAds.setTextColor(Color.parseColor("#3D3D3D"))
            feedback.setTextColor(Color.parseColor("#3D3D3D"))
            privacyPolicy.setTextColor(Color.parseColor("#3D3D3D"))
            root.setBackgroundColor((Color.parseColor("#ffffff")))
            drawerRoot.setBackgroundColor((Color.parseColor("#ffffff")))
            toolbarImage.setColorFilter(Color.parseColor("#3D3D3D"))


            imageTheme.setColorFilter(Color.parseColor("#3d3d3d"))
            imageShare.setColorFilter(Color.parseColor("#3d3d3d"))
//            imageMore.setColorFilter(Color.parseColor("#3d3d3d"))
            imageRate.setColorFilter(Color.parseColor("#3d3d3d"))
            imagePrivacy.setColorFilter(Color.parseColor("#3d3d3d"))
            imageFeedback.setColorFilter(Color.parseColor("#3d3d3d"))
            imageRemoveAd.setColorFilter(Color.parseColor("#3d3d3d"))


        }


    }

    override fun onBackPressed() {
        if (drawerLayout.visibility == View.VISIBLE) {
            hideDrawer()
        } else {
            InterstitialAdsAppLovin.showInterstitial(
                this,
                RemoteKeys.lem_exit_inter,
                object : InterstitialAppLovinCallback {
                    override fun onResult() {
                        Constants.showExitDialog(this@MainActivity)
                    }

                })


//            bg.visibility = View.VISIBLE
        }
    }

    private fun showDrawer() {
        drawerLayout.visibility = View.VISIBLE

        val animation =
            AnimationUtils.loadAnimation(this, R.anim.left_to_right)
        drawerLayout.startAnimation(animation)

    }

    private fun hideDrawer() {


        val animation =
            AnimationUtils.loadAnimation(this, R.anim.right_to_left)
        drawerLayout.startAnimation(animation)
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                drawerLayout.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })


    }


    private fun checkPermission(): Boolean {
        return !(ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED)

        //            ActivityCompat.requestPermissions(
//                this,
//                arrayOf(
//                    Manifest.permission.ACCESS_COARSE_LOCATION,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ),
//                0
//            )
    }


    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        saveValue.isDarkTheme = p1
        refreshTheme()
    }

    private fun rateDialog() {


        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.rate_dialog)
        dialog.window!!.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)

        dialog.submit.setOnClickListener {
            if (dialog.rate.rating > 3) {
                val uri: Uri = Uri.parse("market://details?id=$packageName")
                val myAppLinkToMarket =
                    Intent(Intent.ACTION_VIEW, uri)
                try {
                    startActivity(myAppLinkToMarket)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        this@MainActivity,
                        " unable to find market app",
                        Toast.LENGTH_LONG
                    ).show()
                }
//                Utils.likeApp(this)
            }

        }
        dialog.close.setOnClickListener {
            dialog.dismiss()

        }

        dialog.show()

    }


    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

    fun createMrecAd() {
        if (SharePrefData.getInstance().applovinRectangle.isNotEmpty() && RemoteKeys.lem_applovin_banner == Constants.AL) {
            var adView =
                MaxAdView(SharePrefData.getInstance().applovinRectangle, MaxAdFormat.MREC, this)
            adView!!.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    mediumRectangleAd.visibility=View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    mediumRectangleAd.visibility=View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // MREC width and height are 300 and 250 respectively, on phones and tablets
            val widthPx = AppLovinSdkUtils.dpToPx(this, 300)
            val heightPx = AppLovinSdkUtils.dpToPx(this, 250)

            var params=FrameLayout.LayoutParams(widthPx, heightPx)
            params.gravity=Gravity.CENTER
            adView.layoutParams = params

            // Set background or background color for MRECs to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            adView.gravity= Gravity.CENTER

            mediumRectangleAd.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            mediumRectangleAd.visibility=View.GONE
        }
    }


}