package com.example.liveearthmap

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.liveearthmap.billing.InAppBillingUtils.Companion.setData
import com.example.liveearthmap.other.AdmobInterstitialAds.loadAdmobInterstitial
import com.example.liveearthmap.other.AdmobNatives.loadAdmobNative
import com.example.liveearthmap.BaseActivity
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.airbnb.lottie.LottieAnimationView
import com.example.liveearthmap.billing.InAppBillingUtils
import com.example.liveearthmap.MainActivity
import com.example.liveearthmap.PrivacyActivity
import com.example.liveearthmap.other.*
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.example.liveearthmap.other.AdmobNatives.NativeAdCallback
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : BaseActivity() {
    var remoteConfig: FirebaseRemoteConfig? = null
    var admobNativeView: FrameLayout? = null
    var adlayout: View? = null
    var checkBox: CheckBox? = null
    var privacyText: TextView? = null
    var cTimer: CountDownTimer? = null
    var letsBtn: TextView? = null
    var lottieAnimationView: LottieAnimationView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        setData(this)
        lottieAnimationView = findViewById(R.id.av_from_code)
        admobNativeView = findViewById(R.id.admobNativeView)
        adlayout = findViewById(R.id.adlayout)
        checkBox = findViewById(R.id.checkboxnew)
        privacyText = findViewById(R.id.privacytext)
        checkBox?.setChecked(SharePrefData.getInstance().privacy)
        setUpRemoteConfig()
        letsBtn = findViewById(R.id.lets)
        letsBtn?.setOnClickListener(View.OnClickListener {
           gotoNextScreen()
        })
        privacyText?.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(this@StartActivity, PrivacyActivity::class.java).addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                )
            )
        })
        val text = "I Accept the Privacy Policy"
        val spannable: Spannable = SpannableString(text)
        spannable.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorOrange)),
            13, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        privacyText?.setText(spannable, TextView.BufferType.SPANNABLE)
        setTimer()

        noAdView.setOnClickListener {
            startActivity(Intent(this,AdRemoveActivity::class.java))
        }

        noAdView2.setOnClickListener {
            startActivity(Intent(this,AdRemoveActivity::class.java))
        }
    }

    fun setTimer() {
        cTimer = object : CountDownTimer(8000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                lottieAnimationView!!.visibility = View.GONE
                privacyText!!.visibility = View.VISIBLE
                checkBox!!.visibility = View.VISIBLE
                letsBtn!!.visibility = View.VISIBLE
            }
        }
        cTimer?.start()
    }

    private fun gotoNextScreen(){
        if (checkBox!!.isChecked()) {
            SharePrefData.getInstance().privacy = true

            if (RemoteKeys.lem_splash_inter_new.contains(Constants.AM, true)) {
                AdmobInterstitialAds.showAdmobInterstitial(
                    this,
                    RemoteKeys.lem_splash_inter_new,
                    object : AdmobInterstitialAds.InterstitialAdsCallBack {
                        override fun onResult(showAppLovin:Boolean) {
                            if (RemoteKeys.lem_splash_inter_new == Constants.AM_AL && showAppLovin) {
                                InterstitialAdsAppLovin.showSplashInterstitial(
                                    this@StartActivity,
                                    RemoteKeys.lem_splash_inter_new,
                                    object : InterstitialAppLovinCallback {
                                        override fun onResult() {
                                            startActivity(
                                                Intent(
                                                    this@StartActivity,
                                                    MainActivity::class.java
                                                )
                                            )
                                            finish()
                                        }

                                    })
                            } else {
                                startActivity(
                                    Intent(
                                        this@StartActivity,
                                        MainActivity::class.java
                                    )
                                )
                                finish()
                            }
                        }
                    })
            } else if (RemoteKeys.lem_splash_inter_new == Constants.AL) {
                InterstitialAdsAppLovin.showSplashInterstitial(
                    this,
                    RemoteKeys.lem_splash_inter_new,
                    object : InterstitialAppLovinCallback {
                        override fun onResult() {
                            startActivity(Intent(this@StartActivity, MainActivity::class.java))
                            finish()
                        }

                    })
            }else{
                startActivity(Intent(this@StartActivity, MainActivity::class.java))
                finish()
            }
        } else {
            Toast.makeText(
                this@StartActivity,
                "Please accept our Privacy Policy",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setUpRemoteConfig() {
        remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(1)
            .build()
        remoteConfig!!.setConfigSettingsAsync(configSettings)
        remoteConfig!!.setDefaultsAsync(R.xml.remoteapp)
        remoteConfig!!.fetchAndActivate().addOnCompleteListener { task ->
            if (task.isSuccessful ) {// && !BuildConfig.DEBUG
                remoteConfig!!.activate()
                FirebaseRemoteConfig.getInstance().activate()
                remoteConfig!!.activate()
                SharePrefData.getInstance().admobInter =
                    remoteConfig!!.getString("admob_interstitial_new")
                SharePrefData.getInstance().admobNative =
                    remoteConfig!!.getString("admob_native_new")
                SharePrefData.getInstance().admobInterInner =
                    remoteConfig!!.getString("admob_inter_inner_new")
                SharePrefData.getInstance().admobNativeInner =
                    remoteConfig!!.getString("admob_native_inner_new")
                SharePrefData.getInstance().appLovinInter =
                    remoteConfig!!.getString("applovin_inter")
                SharePrefData.getInstance().appLovinNative =
                    remoteConfig!!.getString("applovin_native")
                SharePrefData.getInstance().applovinRectangle =
                    remoteConfig!!.getString("applovin_rectangle")
                SharePrefData.getInstance().setAppLovinBannerr(remoteConfig!!.getString("applovin_banner"))



                RemoteKeys.lem_splash_native_new = remoteConfig!!.getString("lem_splash_native_new")
                RemoteKeys.lem_dash_native_new = remoteConfig!!.getString("lem_dash_native_new")
                RemoteKeys.lem_permission_native_new = remoteConfig!!.getString("lem_permission_native_new")
                RemoteKeys.lem_wonder_native_new = remoteConfig!!.getString("lem_wonder_native_new")
                RemoteKeys.lem_exit_native_new = remoteConfig!!.getString("lem_exit_native_new")
                RemoteKeys.lem_weather_native_new = remoteConfig!!.getString("lem_weather_native_new")
                RemoteKeys.lem_wonder_inner_native_new = remoteConfig!!.getString("lem_wonder_inner_native_new")
                RemoteKeys.lem_saveaddress_native_new = remoteConfig!!.getString("lem_saveaddress_native_new")

                RemoteKeys.lem_splash_inter_new = remoteConfig!!.getString("lem_splash_inter_new")
                RemoteKeys.lem_speed_inter_new = remoteConfig!!.getString("lem_speed_inter_new")
                RemoteKeys.lem_weather_inter_new = remoteConfig!!.getString("lem_weather_inter_new")
                RemoteKeys.lem_satellite_inter_new = remoteConfig!!.getString("lem_satellite_inter_new")
                RemoteKeys.lem_wonder_inter_new = remoteConfig!!.getString("lem_wonder_inter_new")
                RemoteKeys.lem_shareaddress_inter_new = remoteConfig!!.getString("lem_shareaddress_inter_new")
                RemoteKeys.lem_altitude_inter_new = remoteConfig!!.getString("lem_altitude_inter_new")
                RemoteKeys.lem_compass_inter_new = remoteConfig!!.getString("lem_compass_inter_new")
                RemoteKeys.lem_saveaddress_inter_new = remoteConfig!!.getString("lem_saveaddress_inter_new")
                RemoteKeys.lem_emoji_inter_new = remoteConfig!!.getString("lem_emoji_inter_new")

                RemoteKeys.lem_exit_inter = remoteConfig!!.getString("lem_exit_inter")
                RemoteKeys.lem_premium_exit_inter_new = remoteConfig!!.getString("lem_premium_exit_inter_new")
                RemoteKeys.lem_altitude_exit_inter_new = remoteConfig!!.getString("lem_altitude_exit_inter_new")
                RemoteKeys.lem_compass_exit_inter_new = remoteConfig!!.getString("lem_compass_exit_inter_new")
                RemoteKeys.lem_satellite_exit_inter_new = remoteConfig!!.getString("lem_satellite_exit_inter_new")
                RemoteKeys.lem_saveaddress_exit_inter_new = remoteConfig!!.getString("lem_saveaddress_exit_inter_new")
                RemoteKeys.lem_wonder_exit_inter_new = remoteConfig!!.getString("lem_wonder_exit_inter_new")
                RemoteKeys.lem_shareaddress_exit_inter_new = remoteConfig!!.getString("lem_shareaddress_exit_inter_new")
                RemoteKeys.lem_speed_exit_inter_new = remoteConfig!!.getString("lem_speed_exit_inter_new")
                RemoteKeys.lem_weather_exit_inter_new = remoteConfig!!.getString("lem_weather_exit_inter_new")

                RemoteKeys.splash_native_size = remoteConfig!!.getString("splash_native_size")
                RemoteKeys.lem_applovin_banner = remoteConfig!!.getString("lem_applovin_banner")
                RemoteKeys.lem_applovin_banner_map = remoteConfig!!.getString("lem_applovin_banner_map")


            }
            /*else {
                SharePrefData.getInstance().admobInter =
                    getString(R.string.admob_splash_interstitial)
                SharePrefData.getInstance().admobNative = getString(R.string.admob_native_splash)
                SharePrefData.getInstance().admobInterInner =
                    getString(R.string.admob_splash_interstitial)
                SharePrefData.getInstance().admobNativeInner =
                    getString(R.string.admob_native_splash)
                SharePrefData.getInstance().appLovinInter =
                    getString(R.string.applovin_interstitial)
                SharePrefData.getInstance().appLovinNative =
                    getString(R.string.applovin_native)
                SharePrefData.getInstance().applovinRectangle =
                    getString(R.string.applovin_rectangle)
                SharePrefData.getInstance().setAppLovinBannerr(getString(R.string.applovin_banner))


            }*/

            loadAdmobInterstitial(
                this@StartActivity,
                SharePrefData.getInstance().admobInter.toString().trim()
            )

            if(RemoteKeys.lem_splash_inter_new.contains(Constants.AL,true)) {
                InterstitialAdsAppLovin.loadSplashInterstitial(
                    this,
                    RemoteKeys.lem_splash_inter_new
                )
            }

            when(RemoteKeys.splash_native_size){
                "full"->{
                    globImage.visibility=View.GONE
                    adlayout?.visibility=View.VISIBLE
                    loadnativeAds(admobNativeView!!)
                }
                "medium"->{
                    adlayout?.visibility=View.VISIBLE
                    loadnativeAds(admobNativeView!!)
                }
                "small"->{
                    adlayout2?.visibility=View.VISIBLE
                    loadnativeAds(admobNativeView2!!)
                }
            }

            if(RemoteKeys.lem_dash_native_new.contains(Constants.AM)) {
                NativeAdsDashboard.loadAd(this)
            }
            NativeAdsApplovin.loadNativeAd(this,this)


        }
    }

    private fun loadnativeAds(adView:FrameLayout){

        if(RemoteKeys.lem_splash_native_new.contains(Constants.AM)){

            loadAdmobNative(
                this@StartActivity,
                adView!!,
                RemoteKeys.lem_splash_native_new,
                object : NativeAdCallback {
                    override fun failed() {
                        if(RemoteKeys.lem_splash_native_new==Constants.AM_AL){
                            loadAppLovinNativeAd(adView)
                        }else {
                            mainAdLayout.visibility = View.GONE
                            mainAdLayout2.visibility = View.GONE
                            adlayout!!.visibility = View.GONE
                            adlayout2!!.visibility = View.GONE
                        }
                    }

                    override fun success() {
                        if(RemoteKeys.splash_native_size=="small"){
                            mainAdLayout2.visibility=View.VISIBLE
                            admobNativeView2.visibility=View.VISIBLE
                        }else{
                            mainAdLayout.visibility=View.VISIBLE
                            admobNativeView?.visibility=View.VISIBLE
                        }
                        adlayout!!.visibility = View.GONE
                        adlayout2!!.visibility = View.GONE
                    }
                }, SharePrefData.getInstance().admobNative.toString().trim()
            )

        }else if (RemoteKeys.lem_splash_native_new==Constants.AL){
            loadAppLovinNativeAd(adView)
        }

    }

    private fun loadAppLovinNativeAd(adView:FrameLayout){
        NativeAdsApplovin.splashNativeAds(this,this,adView!!,object :NativeAppLovinCallback{
            override fun onLoaded() {
                if(RemoteKeys.splash_native_size=="small"){
                    mainAdLayout2.visibility=View.VISIBLE
                }else{
                    mainAdLayout.visibility=View.VISIBLE
                }
                adlayout!!.visibility = View.GONE
                adlayout2!!.visibility = View.GONE
            }

            override fun onFailed() {
                mainAdLayout.visibility=View.GONE
                mainAdLayout2.visibility=View.GONE
                adlayout!!.visibility = View.GONE
                adlayout2!!.visibility = View.GONE
            }

        })
    }
}