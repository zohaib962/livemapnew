package com.example.liveearthmap

import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.speech.RecognizerIntent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxAdViewAdListener
import com.applovin.mediation.MaxError
import com.applovin.mediation.ads.MaxAdView
import com.example.liveearthmap.db.DB
import com.example.liveearthmap.db.TaskDao
import com.example.liveearthmap.model.AddressDetail
import com.example.liveearthmap.other.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_share_address.*
import kotlinx.android.synthetic.main.activity_share_address.bannerad
import kotlinx.android.synthetic.main.activity_share_address.currentLocation
import kotlinx.android.synthetic.main.activity_share_address.root
import kotlinx.android.synthetic.main.activity_share_address.zoomIn
import kotlinx.android.synthetic.main.activity_share_address.zoomOut
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.dialog_save_address.*
import kotlinx.android.synthetic.main.search_location.*


class ShareAddressActivity : LocationActivity(), OnMapReadyCallback {
    private lateinit var db: TaskDao

    var count: Int = 0
    var type: Int = 0
    var isFinish: Boolean = false


    override fun updateLocation(location: Location?) {
        if (isShow) {

            return
        }
        if (location != null) {
            this.location = location
            address.text = Utils.getCompleteAddressString(
                applicationContext,
                location.latitude,
                location.longitude
            )
        }


    }

    override fun updateLocationAltidtude(location: LatLng?) {

    }

    var addressDetail: AddressDetail? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_address)
        if (!SharePrefData.getInstance().adS_PREFS) {
            createBannerAd()
        }

        Utils.showMapToast(this)

        if (intent.extras != null && intent.extras!!.containsKey("address")) {
            addressDetail = intent.extras!!.getSerializable("address") as AddressDetail
            isShow = true
            address.text = addressDetail!!.address
            titleText.text = addressDetail!!.name
            locationLayout.visibility = View.GONE
            zoomIn.visibility = View.GONE
            zoomOut.visibility = View.GONE
            currentLocation.visibility = View.GONE
            viewOther.visibility = View.GONE
        } else {
            titleText.text = "Share Address"
        }

        db = DB.get(applicationContext)

        toolbarImage.setImageResource(R.drawable.ic_back)
        toolbarImage.setOnClickListener {
            onBackPressed()
        }

        if (saveValue.isDarkTheme) {
            address.setTextColor(Color.WHITE)
            titleText.setTextColor(Color.WHITE)
            toolbarImage.setColorFilter(Color.WHITE)
            root.setBackgroundColor((Color.parseColor("#000230")))

            bottomRoot.setBackgroundResource(R.drawable.bottom_sheet_bg_night)
        } else {
            bottomRoot.setBackgroundResource(R.drawable.bottom_sheet_bg)

            root.setBackgroundColor(Color.WHITE)
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        copyAddress.setOnClickListener {

            copyAddressToClipBord()

        }

        shareAddress.setOnClickListener {
            shareAddressToOther()

        }

        saveAddress.setOnClickListener {
            saveAddressDialog()

        }

        currentLocation.setOnClickListener {
            goToCurrentLocation()
        }

        zoomIn.setOnClickListener {
            if (mMap != null) {
                mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
            }
        }

        zoomOut.setOnClickListener {
            if (mMap != null) {
                mMap!!.animateCamera(CameraUpdateFactory.zoomOut());
            }

        }


        autoCompleteTextView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                search(p0.toString())

            }

            override fun afterTextChanged(p0: Editable?) {


            }


        })

        bottomRoot.setOnClickListener {

        }

        var bottomSheetBehavior = BottomSheetBehavior.from(bottomRoot);
        bottomSheetBehavior.setHideable(false);//Important to add
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    private fun shareAddressToOther() {

        if (location?.latitude !=null &&  location?.longitude !=null) {
            sharePin()
        } else {
            Toast.makeText(applicationContext, "Address is not ready", Toast.LENGTH_LONG).show()

        }


    }

    fun sharePin() {
        var address = "Link: \nhttps://www.google.com/maps/@${location?.latitude},${location?.longitude},14z"

        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.setType("text/plain")
        sharingIntent.putExtra(
            android.content.Intent.EXTRA_TEXT,
            address + "\nDownload this great app at: https://play.google.com/store/apps/details?id=$packageName"
        )

        startActivity(Intent.createChooser(sharingIntent, "Share Via"))

    }

    private fun saveAddressDialog() {

        if (address.text == "" || address.text == "---") {
            Toast.makeText(applicationContext, "Address is not ready", Toast.LENGTH_LONG).show()
        } else {

            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.dialog_save_address)
            dialog.window!!.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
            dialog.saveAddressDialog.setOnClickListener {
                if (dialog.name.text.toString().isEmpty()) {
                    Toast.makeText(
                        applicationContext,
                        "Enter name of save address!",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setOnClickListener
                }

                var addressDetail = AddressDetail()
                addressDetail.name = dialog.name.text.toString()
                addressDetail.address = address.text.toString()
                if (location?.latitude != null) {
                    addressDetail.latitude = location!!.latitude
                } else {
                    addressDetail.latitude = 0.0
                }
                if (location?.longitude != null) {
                    addressDetail.longitude = location!!.longitude
                } else {
                    addressDetail.longitude = 0.0
                }
                db.insertAddress(addressDetail)

                Toast.makeText(
                    applicationContext,
                    "Address saved!",
                    Toast.LENGTH_SHORT
                ).show()
                dialog.dismiss()

            }
            dialog.show()

        }

    }

    private fun copyAddressToClipBord() {

        if (address.text == "" || address.text == "---") {
            Toast.makeText(applicationContext, "Address is not ready", Toast.LENGTH_LONG).show()
        } else {
            val clipboard: ClipboardManager =
                getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Copy text", address.text)
            clipboard.setPrimaryClip(clip)
            Toast.makeText(applicationContext, "Copy to clipboard", Toast.LENGTH_LONG).show()
        }
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        if (isShow) {
            goToLocation(addressDetail!!.latitude, addressDetail!!.longitude, addressDetail!!.name)
        } else {
            setOnMapClick(googleMap)
        }

    }


    override fun onBackPressed() {

        if (RemoteKeys.lem_shareaddress_exit_inter_new.contains(Constants.AM, true)) {
            AdmobInterstitialAds.showAdmobInterstitial(
                this,
                RemoteKeys.lem_shareaddress_exit_inter_new,
                object : AdmobInterstitialAds.InterstitialAdsCallBack {
                    override fun onResult(showAppLovin: Boolean) {
                        if (RemoteKeys.lem_shareaddress_exit_inter_new == Constants.AM_AL && showAppLovin) {
                            InterstitialAdsAppLovin.showInterstitial(
                                this@ShareAddressActivity,
                                RemoteKeys.lem_shareaddress_exit_inter_new,
                                object : InterstitialAppLovinCallback {
                                    override fun onResult() {
                                        finish()
                                    }

                                })
                        } else {
                            finish()
                        }
                    }
                })
        } else if (RemoteKeys.lem_shareaddress_exit_inter_new == Constants.AL) {
            InterstitialAdsAppLovin.showInterstitial(
                this,
                RemoteKeys.lem_shareaddress_exit_inter_new,
                object : InterstitialAppLovinCallback {
                    override fun onResult() {
                        finish()
                    }

                })
        } else{
            finish()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.REQUEST_CHECK_SETTINGS) {

            startLocation()

        }


    }



    fun createBannerAd() {
        if (SharePrefData.getInstance().applovinBannerr.isNotEmpty() && RemoteKeys.lem_applovin_banner_map == Constants.AL) {
            val adView = MaxAdView(SharePrefData.getInstance().applovinBannerr, this)
            adView.setListener(object : MaxAdViewAdListener {
                override fun onAdLoaded(ad: MaxAd?) {

                }

                override fun onAdDisplayed(ad: MaxAd?) {

                }

                override fun onAdHidden(ad: MaxAd?) {

                }

                override fun onAdClicked(ad: MaxAd?) {

                }

                override fun onAdLoadFailed(adUnitId: String?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdDisplayFailed(ad: MaxAd?, error: MaxError?) {
                    bannerad.visibility = View.GONE
                }

                override fun onAdExpanded(ad: MaxAd?) {

                }

                override fun onAdCollapsed(ad: MaxAd?) {

                }

            })

            // Stretch to the width of the screen for banners to be fully functional
            val width = ViewGroup.LayoutParams.MATCH_PARENT

            // Banner height on phones and tablets is 50 and 90, respectively
            val heightPx = resources.getDimensionPixelSize(R.dimen.banner_height)

            adView.layoutParams = FrameLayout.LayoutParams(width, heightPx)

            // Set background or background color for banners to be fully functional
            adView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

            bannerad.addView(adView)

            // Load the ad
            adView.loadAd()
        } else {
            bannerad.visibility = View.GONE
        }
    }

}