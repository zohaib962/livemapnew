package com.example.liveearthmap

import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.liveearthmap.other.NativeAdsApplovin
import com.example.liveearthmap.other.NativeAppLovinCallback
import com.example.liveearthmap.other.RemoteKeys
import com.example.liveearthmap.other.SharePrefData
import kotlinx.android.synthetic.main.exit_dialog.*
import java.util.ArrayList

class ExitDialog(internal var _activity: Activity) : Dialog(_activity) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exit_dialog)

        NativeAdsApplovin.showNativeAd(_activity,_activity,RemoteKeys.lem_exit_native_new,admobNativeView,object :
            NativeAppLovinCallback {
            override fun onLoaded() {
                mainAdLayout.visibility=View.VISIBLE
            }

            override fun onFailed() {
                mainAdLayout.visibility=View.GONE
            }

        })


        noAdView.setOnClickListener {
            dismiss()
            _activity.startActivity(Intent(_activity,AdRemoveActivity::class.java))
        }

        exitBtn.setOnClickListener {
            dismiss()
            _activity?.finish()
        }

        noBtn.setOnClickListener {
            dismiss()
        }

    }

}