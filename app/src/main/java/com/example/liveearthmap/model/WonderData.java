package com.example.liveearthmap.model;

import java.io.Serializable;

public class WonderData implements Serializable {

    private int icon;
    private String name;
    private String url;
    private double latitude;
    private double longitude;

    public WonderData(int icon, String name,String url , double latitude   , double longitude) {
        this.icon = icon;
        this.name = name;
        this.url = url;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }
}
