package com.example.liveearthmap.model;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.example.liveearthmap.BuildConfig;
import com.example.liveearthmap.R;

import java.util.ArrayList;
import java.util.List;


public class Utils {


    public static boolean checkLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean checkLocationPermission(Context context) {
        return ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static Typeface getDigitalItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "digital-italic.ttf");
    }

    public static void setPadding(View view, int x) {
        view.setPadding(x, 0, 0, 0);
    }

    public static void remove(View view) {
        view.setPadding(0, 0, 0, 0);
    }


    public static void shareApp(Context context) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.app_name));
            String shareMessage = "Get the awesome speed meter with link below!\n";
            shareMessage = shareMessage + "\nhttps://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "Share "));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public static void likeApp(Context context) {

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID));
            context.startActivity(intent);
        } catch (Exception e) {

        }


    }

    public static void moreApp(Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Apps+Entertainment"));
            context.startActivity(intent);
        } catch (Exception e) {

        }

    }

    public static Typeface getTypeFace(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "custom_font.otf");
    }


    public static List<WonderData> getWonderList() {

        List<WonderData> list = new ArrayList<>();

        list.add(new WonderData(R.drawable.seven_wonder_1, "Christ the Redeemer", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Rio%20De%20Janeiro.mp4?alt=media&token=d6a7367f-1920-41dc-bf88-30fa36558e14",
                -22.951878, -43.210624));
        list.add(new WonderData(R.drawable.seven_wonder_2, "Colosseum", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Colosseum.mp4?alt=media&token=89fd0090-2bb7-4025-805d-9830c337c3b8",
                41.889962, 12.492078));
        list.add(new WonderData(R.drawable.seven_wonder_3, "Chichén Itzá", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Chich%C3%A9n%20Itz%C3%A1.mp4?alt=media&token=461a5f6a-b5a1-44e6-bc5d-d23aac64f812",
                20.683366, -88.568507));
        list.add(new WonderData(R.drawable.seven_wonder_4, "Petra", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Petra.mp4?alt=media&token=325f0b8c-9993-4ecc-8ba1-7cedf32c65ce",
                30.328454, 35.444362));
        list.add(new WonderData(R.drawable.seven_wonder_5, "Great Wall of China", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Great%20Wall%20of%20China.mp4?alt=media&token=68c25b5d-9482-485c-a257-67025503f27a",
                40.429541, 116.566143));
        list.add(new WonderData(R.drawable.seven_wonder_6, "Machu Picchu", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Peru.mp4?alt=media&token=5c18dfbe-d771-4215-8e47-8a0e609eff6c",
                -13.163109, -72.544921));
        list.add(new WonderData(R.drawable.seven_wonder_7, "Taj Mahal", "https://firebasestorage.googleapis.com/v0/b/gps-map-81e57.appspot.com/o/Taj%20Mahal.mp4?alt=media&token=cc449e6b-5cac-41ee-9975-4a9d2758e2ec",
                27.175327, 78.042136));

        return list;

    }

}



