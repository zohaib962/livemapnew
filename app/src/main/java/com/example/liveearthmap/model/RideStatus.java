package com.example.liveearthmap.model;

public enum RideStatus {
    PLAY, PAUSE, RESUME, STOP
}
