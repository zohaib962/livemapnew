package com.Translator.pushnotification.service

import android.util.Log
import com.Translator.pushnotification.BuildConfig
import com.google.firebase.iid.FirebaseInstanceId

import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService


open class VoiceIds : FirebaseMessagingService() {


    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        if (BuildConfig.DEBUG) Log.d("Refreshed token: %s", p0!!)
        subscribeToTopic()
    }

    companion object {
        private const val RELEASE_TOPIC = "com.liveearthmaps.gpsnavigation.streetviewlive.maps.navigation"
        private const val DEBUG_TOPIC = "com.liveearthmaps.gpsnavigation.streetviewlive.maps.navigation_test"
        fun subscribeToTopic(): Boolean {
            try {

                if (FirebaseInstanceId.getInstance() != null && FirebaseInstanceId.getInstance().id != null && !FirebaseInstanceId.getInstance().id.isEmpty()) {
                    if (BuildConfig.DEBUG)
                        FirebaseMessaging.getInstance().subscribeToTopic(DEBUG_TOPIC)
                    else {
                        FirebaseMessaging.getInstance().subscribeToTopic(RELEASE_TOPIC)
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(DEBUG_TOPIC)
                    }
                    return true
                }

            } catch (e: Exception) {
//                Log.e("FirebaseMessaging", e.toString())
            }
            return false
        }
    }

}
